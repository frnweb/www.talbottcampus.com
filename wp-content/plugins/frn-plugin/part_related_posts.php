<?php 

/*
	/////////
	/// Related Posts Shortcode & Function
	/////////
	Initially created by Daxon Edwards 6/10/2016
	Transitioned to functions and shortcode 2/8/17
	Added automatic option July 2017
	Added custom post options August 2017
	v=2.3.1 (fixed issue calling ACF function when ACF not installed)
	v=2.3.2 (added citations to the list of sources header text to look for) - 9/12/17
	v=2.3.3 (fixed issue with "searchbox" option for HTML. It included introductory text when it shouldn't.) - 10/17/17
	v=2.3.4 - 12/11/17
		- fixed issue with default styles not being disabled--was written for radio buttons instead of checkbox--null values how handled
		- moved toc h_level outside of styles since used in printing of the list anyway (learned from us on BlackBear)
		- added li margin and padding options if available in ACF fields
		- added post-specific search term capability via ACF
	v=2.3.5 - 1/31/18 - 2/2/18
		- removed border by default and made sure the global option is working
		- added consideration for global settings in the FRN plugin main settings (post-specific still overrides)
		- added "inner" wrapper around related posts for all situations.
		- improved related posts HTmL structure
		- added strip_tags function to strip html from titles (problem noticed on FRN.com with italics in Recovery Unscripted entries)
	v=2.3.6 - 6/27/18 - added frn_toc_anchor to a name for page anchors when TOC links to related posts. This adds spacing at the top for stationary menus.
	v=2.3.7 - 6/5/19 - Fixed relevanssi query to updated array-style; fixed problem with post_type always defaulting to any even though admin selected specific ones.
*/


/*
	CONTENTS: 
	* SHARED FUNCTIONS (search phrase prep and search array build)
	* SHORTCODE FUNCTIONS
	* SHORTCODE DISPLAY LIST FUNCTION
	* Auto RP - Core Function (adds the list to content)
	* Auto RP - Trigger (Decides what page should get the list added)

	Test Scenario:
		404 shortcode (all html attributes)
		regular shortcode:
			test attributes
		auto placement:
			post types
			with sources
			without sources
			styling overwrites

*/




/////////////////
// SHARED FUNCTIONS
////////////////


////////
//Core search function. It simply returns an array
//Used for shortcode and auto related posts
if(!function_exists('frn_related_search')) {
function frn_related_search($total="", $search="", $post_type="") {

	$rp = get_option('site_rp');

	//var prep
	$s = frn_url_search_prep($search);

	//Set default number or recommended posts
	$total = trim($total);
	if($total=="" || !is_numeric($total)) {
		if(is_404()) $total=8;
		else $total=3;
	}

	$unique_id="";
	if(frn_check_if_post()=="yes") $unique_id=get_the_id();
	$search_array=false;
	if(!isset($rp['cache'])) $rp['cache']="";
	//echo "Dax cache: ".$rp['cache'];
	if(!is_404() && $unique_id!=="") {
		//if array, then it's likely not a 404 page. The cache can be helpful.
		//if array, it's used in a webpage and not 404. As a result, caching can be helpful.
		if($rp['cache']=="") $search_array=get_transient( 'RP_search_'.$unique_id );
	}

	if($search_array===false) {
		$post_type=trim($post_type);
		
		//if(!is_404()) {
			if(!isset($rp['search_types'])) $rp['search_types']="";
			if($post_type=="" && $rp['search_types']!=="") $post_type=$rp['search_types']; //overrides only if "any" is used
		//}
		
		//if post_type is a string, turn it into an array instead
		//if post_type is blank, search will only include posts -- WP default
		if($post_type!=="any" && !is_array($post_type)) $post_type=explode(",",$post_type);
		//echo "<h1>Dax Before: ";
		//print_r($rp['search_types']);
		//print_r($post_type);
		//echo "</h1>";
		//Remove "any" if it's in an array since we don't want it to mess up the wp_query
		//remove admin's radio "any" option so that attachements, etc. don't show up. Our users naturally think "any" means pages and posts and content post types.
		if(is_array($post_type)) {
			if(in_array("any",$post_type)) {
				$any_id=count($post_type)-1;
				unset($post_type[$any_id]); //assumes "any" is always at the end of any array -- because of checkbox order in settings
			}
		}
		//echo "<h1>After: ";
		//print_r($post_type);
		//echo "</h1>";
		
		if(!function_exists('relevanssi_do_query')) {
			// another test option for plugins: 
			//   using the plugin's detection of activated plugins based on their main PHP in WP's list of activated plugins
			//   frn_test_plugin_activation('relevanssi/relevanssi.php'); //boolean
			//Defaut WP search approach

			//ADD ONE MORE TO QUANTITY RETURNED
			//"post__not_in" was not working for a few months between Fall 2018 and Winter 2019. 
			//As a result, the current page was always showing in the list. 
			//Manually removed the current page in the list of results when printing the list to a page.
			//But in order to only show our preferred quantity, we have to increment it here.
			//Once WP fixes this issue, it means we'll have one more post showing in the lists, so we'll have to programmatically remove this.
			$total++; 

			$args = array( 
				's' 				=> $s, //"treatment", 
				'post_status' 		=> 'publish',
				'post_type' 		=> 'any',
				'posts_per_page' 	=> $total,
				'fields'			=> 'ids'
			);
			if(!is_404()) {
				if($post_type!=="") {
					$args['post_type'] = $post_type; //can be "any" or an array
				}
				if($unique_id!=="") {
					$args['post__not_in'] = array($unique_id);
				}
			}
			//print_r($args);
			//echo "<h1>Dax Test</h1>";
			$search_array = new WP_Query($args);
			//print_r($search_array);
		}
		else {
			//Relevanssi search approach
			/*
			$search_array = new WP_Query();
			$search_array->query_vars['s']				=$s;
			$search_array->query_vars['posts_per_page']	=$total;
			$search_array->query_vars['paged']			=0;
			$search_array->query_vars['post_status']	='publish';
			$search_array->query_vars['fields']			='ids'; 
			//including this throws a PHP error in the Relevanssi plugin. It expects objects, but instead I request the system return an array of IDs to avoid unnecessary processing and loops. 
			//There is no way around this. The system will freeze if we don't use IDs. As of 10/17/17
			if($post_type!=="") $search_array->query_vars['post_type']		= $post_type;
			if($unique_id!=="") $search_array->query_vars['post__not_in']	= array($unique_id); 
			*/


			$args = array(
			    's' => $s,
			    'posts_per_page' => $total,
			    'paged'			=> 0,
				'post_status'	=> 'publish',
				'fields'		=> 'ids'
			);
			if(!is_404()) {
				if($post_type!=="") {
					$args['post_type'] = $post_type; //can be "any" or an array
				}
				if($unique_id!=="") {
					$args['post__not_in'] = array($unique_id);
				}
			}
			$search_array = new WP_Query();
			$search_array->parse_query( $args );
			//print_r($search_array);
			//echo "<h1>Dax Test</h1>";
			relevanssi_do_query( $search_array );
			//print_r($search_array);
		}

		

	}


	if(!is_404()) {
		// Caching
		//if array, it's used in a webpage and not 404. As a result, caching can be helpful.
		//set caching var to avoid the process heavy Relevanssi search for every load when page content doesn't change often.
		//if caching turned off, we will still save the variable down below for once they reactivate caching.
		$time_limit=60*60*4; //cache timeout in seconds (sec x min x hours) -- refresh is every four hours
		if($search_array) {
			if ( $search_array->have_posts() ) {
				set_transient( "RP_search_".$unique_id, $search_array, $time_limit ); 
			}
		}
	}

	wp_reset_postdata();

	return $search_array;

}
}


////////////
//// Prepping Search Terms
//   Used by shortcode and auto function (looped into the main list building functions)
if(!function_exists('frn_url_search_prep')) {
function frn_url_search_prep($search="") {

	$search=trim($search); $s="";

	//echo "Provided Phrase: ".$search."<br />";

	// 1. If search term passed to function (e.g. shortcode attribute)
	if($search!=="") {
		$s = $search; //use a manually entered set of keywords
	}
	else {

		// 2. If shortcode used on a post and site-wide search term provided
		//	  We don't want the 404 page showing the sitewide term (i.e. not helpful to user)
		if(!is_404()) {
			//ACF option overrides global option
			if(function_exists('get_field') && frn_check_if_post()=="yes") {
				$term = get_field('frn_rp_term');
				if($term) {
					$s=trim($term);
				}
			}
			//if post-specific option not available, check for global
			if(trim($s)=="") {
				$rp = get_option('site_rp');
				if($rp) {
					$s=$rp['term'];
				}
			}
		}
		if(trim($s)=="") {
			// 3. No search term provided, use words in URL instead
			$check_end_slash = substr($_SERVER['REQUEST_URI'],-1);
			if($check_end_slash=="/") $uri_without_ending_slash = substr($_SERVER['REQUEST_URI'],0,strlen($_SERVER['REQUEST_URI'])-1);
			else $uri_without_ending_slash = $_SERVER['REQUEST_URI'];
			$first_slash_from_end_pos = strrchr($uri_without_ending_slash,"/");
			//echo "<b>".$first_slash_from_end_pos."</b><br />";
			//$last_part_of_uri = substr($uri_without_ending_slash,$first_slash_from_end_pos);
			$s = str_replace("/","",$first_slash_from_end_pos);
			$s = urldecode($s);
			$s = str_replace("-"," ",$s);
			$s = str_replace("_"," ",$s);
			$s = trim(strtolower(preg_replace('/[0-9]+/', '', $s ))); //remove all numbers
		}
	}

	//echo "Final Phrase: ".$s."<br />";
	
	$stop_words = array(
		"for",
		"the",
		"and",
		"an",
		"a",
		"is",
		"are",
		"than",
		"that",
		"I",
		"to",
		"on",
		"it",
		"with",
		"can",
		"be",
		"of",
		"get",
		"in",
		"you",
		"from",
		"if",
		"by",
		"so",
		"at",
		"do",
		"&",
		"there",
		"too"
	);
	$i=1;
	foreach($stop_words as $word){
		/*
		//for testing:
		if($i==1) {
			echo $s."<br />";
			echo $word."<br />";
			echo "string: ".strlen($s);
			echo "; word: ".strlen(" ".$word)."<br />";
			echo "position: ".strpos($s,$word." ")."<br />";
			echo "string without word: ".(strlen($s)-strlen(" ".$word))."<br />";
		}
		*/
		$word = trim(strtolower($word));
		$s = str_replace(" ".$word." "," ",$s); ///in the middle
		if(strpos($s,$word." ")===0) $s = str_replace($word." ","",$s); // at the beginning
		if(strpos($s," ".$word)==strlen($s)-strlen(" ".$word)) $s = str_replace(" ".$word,"",$s); // at the end
		$i++;
	}
	$s=trim($s);
	if($s=="") $s="treatment";

	return $s;

}
}

/*
if(!function_exists('frn_manual_list')) {
function frn_manual_list() {
	//if there is a manual list of posts, store the list in the var
	$frn_list=""; $manual_rps=false;
	if(function_exists('get_field') && frn_check_if_post()=="yes") {
		$styling = get_field('frn_rp_styling');
		$manual_rps=get_field('frn_rp_manual_posts');
	}
	//echo "<h1>Dax: ".count($manual_rps)."</h1>";
	//print_r($manual_rps);

	if($manual_rps) {
		//echo "<h1>".count($manual_rps)."</h1>";
	    foreach( $manual_rps as $post_item): 
	        //setup_postdata($post); 
	        $title=htmlspecialchars(strip_tags($post_item->post_title));
	        $frn_list .= "
			<li>
				<a href=\"".get_the_permalink($post_item->ID)."\" onClick=\"frn_reporting('','Content Interactions', 'Related Posts', '".$title."',false,''); \">".$title."</a>
			</li>
			";
	    endforeach; 
	    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
	}

    return $frn_list;

}
}
*/







/////////////////
// SHORTCODES
////////////////



///////////
// New Version

//Benefits:
// If function not available, then it displays the code instead of a failure.
// The best approach is to use the function, but wrap it in the function_exists function so that things fail nicely even if the plugin isn't used.
// [frn_related_list html="" total="" search="" no_results_msg=""]
// Easier to remember version of shortcode 6/20/17
if(!function_exists('frn_related_shortcode')) {
function frn_related_shortcode($atts) {

		extract( shortcode_atts( array(
			'html' => "",
			'total' => "",
			'search' => "",
			'no_results_msg' => "",
			'post_type' => "any",
			'class' => "",
			'id' => ""
			), $atts, 'frn_related' ) );

		return frn_related_sc_funct($html, $total, $search, $no_results_msg, $post_type, $class, $id);

}}
add_shortcode( 'frn_related', 'frn_related_shortcode' );





///////////
// Old Version
//The following is the initial version of the shortcode discontinued 6/2017, but some facilities may still use this in their 404 pages
if(!function_exists('frn_related_list_shortcode')) {
function frn_related_list_shortcode($atts) {

		extract( shortcode_atts( array(
			'html' => "",
			'total' => "",
			'search' => "",
			'no_results_msg' => "",
			'post_type' => "any",
			'class' => "",
			'id' => ""
			), $atts, 'frn_related_list' ) );

		return frn_related_sc_funct($html, $total, $search, $no_results_msg, $post_type, $class, $id);

}}
add_shortcode( 'frn_related_list', 'frn_related_list_shortcode' );





/////////
// SHORTCODE LAYOUT
// Determines how the search results are to be displayed
// Only used in shortcode situations
if(!function_exists('frn_related_sc_funct')) {
function frn_related_sc_funct($html="", $total="", $search="", $no_results_msg="", $post_type="", $ul_class="frn_suggestions", $ul_id="") {
	
	$html = strtolower($html);
	$rp = get_option('site_rp');

	/*
		OVERVIEW OF STEPS in this function:
			1. Build list of related posts
				a. check for manual list
				b. search for related posts
			2. Prepare 404 Page Display
			3. Prepare post/page display
	*/



	 ////////
	 /// PART ONE: ARRAY RETURN (html=="array")
	 ///////

	     // Just return an array so that a custom function can be used to prepare the list
	     //If manual posts are provided on a post page, that option will be ignored.
		 // This might be used on only one site...maybe
	     if($html=="array") {

	     	//Since it's just returning the array, do it all here instead of relying on frn_rp_list
	     	$search_words = frn_url_search_prep($search);
			$search_array=false;
			if($search_words!=="") {
				
				if($html=="") {
					//Overrides defaults since this might be used with auto RPs

					if(!isset($rp['search_types'])) $rp['search_types']="";
					if(!isset($rp['count'])) $rp['count']="";

					if($rp['search_types']!=="") {
						$post_type = $rp['search_types'];
					}
				}
				
				//Shortcode override main settings and defaults
				if($total!=="") {
					$rp['count']=$total;
				}

				// Loads array of post IDs returned by search
				$search_array=frn_related_search($rp['count'], $search_words, $post_type);

			}

	     	if($search_array->have_posts()) return $search_array;
	     	else return FALSE; 

	     }





	 ////////
	 /// PART TWO: PREPARE 404 DISPLAY
	 ///////
	     // (if html==array, then function wouldn't get to this point)




     	////
	 	// STEP ONE: BUILD LIST OF POSTS
	    // Creates list wrapped in UL and LIs
	    if($html!=="" && $total=="") $total=8; //html isn't blank on 404 pages, which by default shows 8 posts
		$frn_list = frn_rp_list($total,$search,$post_type,$ul_class,$ul_id);
	     




	    //////
		// STEP TWO: 404 CONTENT

		$four_o_four_block="";
	    $rp_wrapped="";

	    if($html=="404") {
	     	// return all the typical 404 content

	     	/* 
	     	//5/22/17 - decided to remove since it was making design challenging to consider times when nothing is returned
	     	<h1>Woops! The Web Address is Incorrect</h1>
	     	*/

	     	
	     	
	    	if(trim($frn_list)!=="") {

			    $four_o_four_block .="
			    <p>It looks like something may be wrong with the web address you used.</p>

			    <h2>Were you looking for any of these?</h2>
				".$frn_list."
				<br />
				";

		 	}
		 	else {

				// if there are no posts, display this message:
				if($no_results_msg!=="") $four_o_four_block .= $no_results_msg;
				else { 
					$four_o_four_block .= "
				<p>
					And unfortunately, we can't find any posts that might work. 
					Take a look at it in the address bar of your browser above and see if the web address looks pretty normal. 
					Make corrections if not and try again.
				</p>
				";
				}
			}

			//Add search box
			$four_o_four_block .="
				<p><b>...or maybe try searching:</b></p>
		        <div class=\"frn_search_box\">".get_search_form(false)."</div>
				";
		 }



		 //////
		 // LIST AND SEARCHBOX
		 elseif($html=="searchbox") {
	     	// return ONLY the list and a search box
	     	// includes statement about looking for these since that statement should be there when nothing is returned. That can't be controlled without running function.
	     	// No default 404 intro message

	    	if(trim($frn_list)!=="") {
				$four_o_four_block .= "<h3>Were you looking for any of these?</h3>".
				$frn_list."
				<br />
				
				";
			}  
			else { 
				// if there are no posts, display this message:
				if($no_results_msg!=="") {
					$four_o_four_block .= $no_results_msg;
				}
				else { 
					$four_o_four_block .= "
				<p>
					And unfortunately, we can't find any posts that relate to it. 
					Take a look at it in the address bar of your browser above and see if the web address looks pretty normal. 
					Make corrections if not and try again.
				</p>
				";
				}
			}
			
			$four_o_four_block .="
				<p><b>...or maybe try searching:</b></p>
		        <div class=\"frn_search_box\">".get_search_form(false)."</div>
				";

		 } // ends html=searchbox






		 ////////
		 /// STEP THREE: PREPARE POSTS/PAGES DISPLAY
		 ///////
		 /*
		 // Basically, this is the version manually inserted into posts/pages
		 // Not necessarily a step, but this should match the frn_auto_related_posts formatting
		 // PROCESS:
		 	1. Load page/post style of list
		 	2. 
		 	3. 
		 */

		 else { 

		 	if(trim($frn_list)!=="") {
		 		
		 		/*
		 		//REVIEW:
	 				* frn_list includes a UL/LI list of related posts, but they have no default styling or DIV
	 				* We need to load:
	 					the styles, and 
	 					wrap the UL & styles with the DIV planned for posts/pages
				*/

	 			$rp_styled = "";
	 			if(frn_check_if_post()=="yes") {
			 		// Load STYLED version of HTML for posts/pages:
			 		//	Adds a STYLE CSS settings
			 		//	Incorporates all versions of visual customizations
				    $rp_styled = frn_rp_style($frn_list);
				}
				
				if(trim($rp_styled)!=="") {
				// DIV WRAPPER CUSTOMIZATIONS:
				    $styling=false; 
					if(function_exists('get_field')) {
						// another test option for plugins: 
						//   using the plugin's detection of activated plugins based on their main PHP in WP's list of activated plugins
						//   frn_test_plugin_activation('advanced-custom-fields-pro/acf.php'); //boolean
						$styling = get_field('frn_rp_styling');
					}

			    	// CLASS
			    		// 1. CLASS attribute provided by shortcode
			    		//    But if it's blank...
			    		if(trim($class)=="") {
			    			// 2. ACF post settings (priority over overall settings and PHP default)
							if($styling) {
								$class=trim($styling['class']);
							}
							// 3. FRN Settings (priority over PHP default)
							if(trim($class)=="") {
								//if still blank
							    if(!isset($rp['class'])) $rp['class']="";
							    if($rp['class']!=="") $class=$rp['class'];
						    }
						    // 3. SYSTEM Default
						    if(trim($class)=="") {
						    	//if still blank
							    $class="frn_related_posts";
							}
						}


			    	// ID
						// 1. ID attribute provided by shortcode
			    		//    But if it's blank...
			    		if(trim($id)=="") {
			    			// 2. ACF post settings (priority over overall settings and PHP default)
							if($styling) {
								$id=trim($styling['id']);
							}
							// 3. FRN Settings (priority over PHP default)
							if(trim($id)=="") {
								//if still blank
							    if(!isset($rp['id'])) $rp['id']="";
							    if($rp['id']!=="") $id=$rp['id'];
						    }
						    // 3. SYSTEM Default
						    if(trim($id)=="") {
						    	//if still blank
							    $id="frn_related_posts";
							}
						}


					$rp_wrapped = "
			<div id=\"".$id."\" class=\"".$class."\">
				<div class=\"".$class."_inner\">"
					.$rp_styled."
				</div>
			</div>
			<br />
			";
				}
				else {
					$rp_wrapped = "<!-- No related posts available -->";
				}


			}
			else {
				//no_results_msg only provided via shortcode
				//if on post, only the comment is displayed mostly as a confirmation
				if($no_results_msg=="blank") $rp_wrapped = "";
				elseif($no_results_msg!=="") $rp_wrapped = $no_results_msg;
										else $rp_wrapped = "<!-- No related posts available -->";
			}
			
		 }

	
	



	 /////////
	 //// STEP THREE: RETURN THE LIST
	 /////////
	 // 404_block is blank if HTML is blank
	 // rp_wrapped is blank if HTML attribute isn't
	 return $four_o_four_block.$rp_wrapped;
	
}}




///////////
// LIST PREPARATION - Used in shortcode and auto functions
///////////

function frn_rp_list($total="", $search="", $post_types="", $ul_class="",$ul_id="") {
	/*
	//This function prepares ONLY the list of URLs without any styling
	// PROCESS:
		1. check for manual list
		2. search for automatic list
		3. wrap with UL or OL
		4. return list
	*/
	
	$frn_list=""; 
	$post_page=frn_check_if_post();


	/////
	// PART ONE: Prepare the list
	////


	// 1. MANUAL LIST:
	//Check and load current post custom fields for formatting related posts
 	//Expects ACF to be set up using these field vars
	if(function_exists('get_field') && $post_page=="yes") {
		//if there is a manual list of posts, store the list in the var
		$manual_rps=FALSE; $styling=FALSE;
		$styling = get_field('frn_rp_styling');
		$manual_rps=get_field('frn_rp_manual_posts');
		
		//echo "<h1>Dax: ".count($manual_rps)."</h1>";
		//print_r($manual_rps);

		if($manual_rps) {
			//echo "<h1>".count($manual_rps)."</h1>";
		    foreach( $manual_rps as $post_item): 
		        //setup_postdata($post); 
		        //echo "[ID: ".$post_item->ID."] ";
		        $title=strip_tags($post_item->post_title);
		        $frn_list .= "
						<li>
							<a href=\"".get_the_permalink($post_item->ID)."\" onClick=\"frn_reporting('','Content Interactions', 'Related Posts', '".$title."','no'); \">".$title."</a>
						</li>";
		    endforeach; 
		    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
		}
	}



	// 2. URL SEARCH RESULTS
	//check if current post has a manual list of related posts provided
	//if so, skip search and make sure manual list is in a proper array format
	if(trim($frn_list)=="") {

		// 1. Prepare words for searching
		$search_words = frn_url_search_prep($search); //always returns something
		
		// 2. Do the search (i.e. prepare the array)
		if($search_words!=="") {
			//defaults for each of these are provided in the function (i.e. no need to define here)
			$search_array=frn_related_search($total, $search_words, $post_types); //always returns something
		}
		
		// 3. Create the list (li only)
		$loop=0;
		if($search_array) {
			if ( $search_array->have_posts() ) {

				$current_page_id="";
				if($post_page=="yes") $current_page_id = get_the_id();

				//print_r($search_array);

				//Get ID of current post before looping through results, but only if the standard WP search is enabled
				//sometimes other plugins overwrite the posts__not_in wp_query var and the current post is still in results (Talbott site has this issue as of 7/24/17)
				while ( $search_array->have_posts() ) {
					$search_array->the_post();
					if(get_the_id()!==$current_page_id) {
						$title=strip_tags(get_the_title());
						$loop++;
						$frn_list .="
						<li>
							<a href=\"".get_the_permalink()."\" onClick=\"frn_reporting('','Content Interactions', 'Related Posts', '".$title."',false,''); \">".$title."</a>
						</li>";
					}
				}
				wp_reset_postdata();
			}
		}
		//if the same page was in results and it was the only one, then frn_list isn't blank but nothing should be returned. This makes sure that happens.
		if($loop==0) return "";

	}



	/////
	// PART TWO: Wrap the list
	////

	$list_style=""; $type="ul";
	if($post_page=="yes" && function_exists('get_field')) {
		//if we add UL here, the following is needed:
			//if false isn't returned, then there is a list and wrapped it in ULs
			//check for post-specific styling using ACF plugin
		$styling=false; 
		$styling = get_field('frn_rp_styling');
		if($styling) {
			//Even if class sent to this function, it'll be overwritten by the post-specific setting
			if($ul_class=="") $ul_class=trim($styling['ul_class']);
			$list_style=$styling['list_style'];
		}
		if($list_style=="ol") $type = "ol";
	}
	if($ul_class=="") $ul_class="frn_rp_results";
	if($ul_class!=="") $ul_class=" class=\"".$ul_class."\"";
	if($ul_id!=="") $ul_id=" id=\"".$ul_id."\"";
	
	//note: redefines frn_list
	$frn_list = "
					<".$type.$ul_id.$ul_class.">
						".$frn_list."
					</".$type.">
					";



	/////
	// PART THREE: Return the list
	////

	return $frn_list;

}







///////////
// STYLE THE RP LIST
///////////

if(!function_exists('frn_rp_style')) {
function frn_rp_style($frn_list="") {
	//This function includes the CSS in-page styles, adds an h2 header, and page anchor
	//This portion is used for auto RPs and as the default shortcode approach

	$rp = get_option('site_rp');
	$rp_block="";

	if(trim($frn_list)!=="") {

		$styles="";
		if(!isset($rp['default_styles'])) $rp['default_styles']="";
		//Check and load current post custom fields for formatting related posts
	 	//Expects ACF to be set up using these field vars
		$styling=false; $title_overrides=false;

		if(function_exists('get_field') && frn_check_if_post()=="yes") {
			$styling = get_field('frn_rp_styling');
			$title_overrides=get_field('frn_rp_title');
		}

		//using the TOC header levels since RPs header level should be the same
		$toc = get_option('site_toc');
		if(!isset($toc['h_level'])) $toc['h_level']="h2";
		elseif($toc['h_level']=="") $toc['h_level']="h2";

		if($rp['default_styles']=="") {

			$float=""; $margin=""; $padding=""; $width=""; $maxwidth=""; $border=""; $list_style=""; $ul="ul";
			$ul_padding=""; $ul_margin=""; $li_margin=""; $li_padding="";
			$class=""; $div_id="";
			
			//load individual post settings from ACF if set
			if($styling) { 
				//newer styles added later (i.e. styling may be set but these won't in ACF)
				if(!isset($styling['maxwidth'])) $styling['maxwidth']="";
				if(!isset($styling['ul_margin'])) $styling['ul_margin']="";
				$float=trim($styling['float']);
				$margin=trim($styling['margin']);
				$padding=trim($styling['padding']);
				$border=trim($styling['border']);
				$list_style=$styling['list_style'];
				$width=trim($styling['width']);
				$maxwidth=trim($styling['maxwidth']);
				$class=trim($styling['class']);
				$ul_padding=trim($styling['ul_padding']);
				$ul_margin=trim($styling['ul_margin']);
				$li_margin=trim($styling['li_margin']);
				$li_padding=trim($styling['li_padding']);
			}


			//load overall FRN Settings if custom any ACF setting is blank
			if($float=="") $float=trim($rp['float']);
			if($margin=="") $margin=trim($rp['margin']);
			if($padding=="") $padding=trim($rp['padding']);
			if($border=="") $border=trim($rp['border']);
			if($list_style=="") $list_style=$rp['list_style'];
			if($width=="") $width=trim($rp['width']);
			if($maxwidth=="") $maxwidth=trim($rp['maxwidth']);
			if($class=="") $class=trim($rp['class']);
			if($ul_padding=="") $ul_padding=trim($rp['ul_padding']);
			if($ul_margin=="") $ul_margin=trim($rp['ul_margin']);
			if($li_margin=="") $li_margin=trim($rp['li_margin']);
			if($li_padding=="") $li_padding=trim($rp['li_padding']);
			

			//set defaults
			if($float=="") {
				$float="left";
			}
			//elseif($float!=="none") { 
			//	$width="100%";
			//}
			if($margin=="") {
				$margin="40px 0 60px 0";
			}
			if($padding=="") {
				$padding="0 0 60px 0";
			}
			if($width=="") {
				$width="100%";
			}
			/* //no use case for this yet. Once there is, we can activate it.
			//maxwidth fix
			$width_test = str_replace("%","",$width);
			$maxwidth_test = str_replace("%","",$maxwidth);
			$width_test = str_replace("px","",$width);
			$maxwidth_test = str_replace("px","",$maxwidth);
			if(trim($width_test) > trim($maxwidth_test)) {
				$maxwidth = $width;
			}
			*/
			if($border!=="") {
				$border="border:".$border.";\n";
			}
			else {
				$border="border-bottom:1px solid #ccc;\n";
			}
			if($list_style!=="") {
				//four options: none, bullets (disc), numbers (ol), or circles
				if($list_style!=="ol") {
					if($list_style=="none" && $ul_padding=="") {
						$ul_padding=".5em .5em 0 0";
					}
					$list_style = "
					list-style: ".$list_style.";";
				}
				else {
					$ul="ol";
				}
			}
			if($class=="") {
				if(!isset($rp['class'])) $rp['class']="";
				if(trim($rp['class'])!=="") $class=$rp['class'];
				else $class="frn_related_posts";
			}
			if($ul_padding=="") {
				$ul_padding=".5em .5em 0 1.1em";
			}

			if($ul_margin=="") {
				$ul_margin="0";
			}

			if($li_margin=="") {
				$li_margin="margin-bottom: .8em";
			}
			else {
				$li_margin="margin: ".$li_margin;
			}

			if($li_padding=="") {
				$li_padding="5px 5px 5px 0";
			}

			$styles="
			<style>
				.".$class." {
				    margin: ".$margin.";
				    width: ".$width.";
				    float: ".$float.";
				    ".$border."padding: ".$padding.";
				}
				.".$class." ".$ul." {
				    margin: ".$ul_margin.";
				    padding: ".$ul_padding.";".$list_style."
    			}
    			.".$class." li {
				    ".$li_margin.";
				    padding: ".$li_padding.";
    			}
    			.".$class." ".$toc['h_level']." {
					padding: 0;
					margin: 0;
				}
				.frn_clear {
					clear:both;
				}
				@media (max-width: 1023px) {
					.".$class." {
					    float: none;
					    max-width:100%;
					    width:inherit;
					}
					.".$class." ".$toc['h_level']." {
						padding: 0 0 5px 0;
						margin: 0;
					}
					.".$class." ".$ul." {
					    padding: 0;
					    list-style: none;
					}
					.".$class." li {
					    margin: 10px 0;
					}
					.".$class." li:active {
						background-color: #efefef;
					}
				}
			</style>
			";
		}


		//Add CSS styles, anchor link and header
			//Styling is added here since if positioning with a DOM, it needs to create the DIV wrapper. 
			//It's much easier to put the styles inside the DIV, although unconventional.
		//Testing:
			//echo "<h1>Title: ";
			//print_r($title_overrides);
			//echo "</h1>";

		$title="";
		if($title_overrides) {
			global $frn_mobile;
			if($frn_mobile=="Smartphone") {
				if(trim($title_overrides['smartphones'])!=="") $title=$title_overrides['smartphones'];
			}
			else {
				if(trim($title_overrides['desktops'])!=="") $title=$title_overrides['desktops'];
			}
		}

		if($title=="") {
			if(!isset($rp['h2'])) $rp['h2']="";
			if(trim($rp['h2'])!=="") {
				$title=$rp['h2'];
			}
			else {
				//Default title
				$title="Continue Reading";
			}
		}

		//using the TOC header levels since RPs header level should match
		$rp_block = $styles."
					<a name=\"frn_related_posts\" class=\"frn_toc_anchor\"></a>
					<".$toc['h_level']." id=\"frn_related_hdr\">".$title."</".$toc['h_level'].">
					".$frn_list."
					";

	}

	return $rp_block;

}}






////////////
/////
//// AUTOMATIC RELATED POSTS FUNCTIONS
/////
/////////////




///////////
// AUTOMATIC RP - CORE Element (Adds and Builds List)
///////////

add_action( 'the_content', 'frn_auto_related_posts', 10 );
if(!function_exists('frn_auto_related_posts')) {
function frn_auto_related_posts($content) {
	//This is the main "automatic" function controlling the searching, preparation, and placement of RPs in the content

	if(frn_auto_rp_trigger($content)) {

		// 1. Get the list
		//Pulls either manual or automatic options for posts
		//No need to define vars since they all have defaults handled within the function
		$frn_list = frn_rp_list(); 

		if(trim($frn_list)!=="") {
			
		// 2. Style the list
			//style and build the final block
			$rp_block = frn_rp_style($frn_list);

		// 3. Position it
			//add and position it in the content
			return frn_auto_rp_placement($content,$rp_block);
		}

	}

	// Return content if anything fails above
	return $content;

}
}





///////////
// AUTOMATICE RP - TRIGGER Evaluation for Auto RP
///////////

function frn_auto_rp_trigger($content) {
	//This function is called upon within the AUTO RP function above. 
	//Simply returns TRUE if a page should get an RP list or FALSE if it should not.

	//The following two items are necessary for any "the_content" filter
	if( in_the_loop() && is_main_query() ) {

		
		//the_content hook affects excerpts, meta descriptions, etc. 
		//By adding these two checks, it'll make sure that this feature only changes primary content displays and nothing else.
		$rp = get_option('site_rp');
		if(!$rp) return FALSE;
		elseif(stripos($content,"frn_related")) return FALSE; //if shortcode already in content, deactivate the auto feature -- assumes we wouldn't want two cases on the same page although they could use different searches
		else {

			if(!isset($rp['activation'])) $rp['activation']="";
			if($rp['activation']!=="yes") return FALSE;
			else {

				//Since it's activated, go through normal checks
				//In order of most common
				if(is_single() || is_page()) {

					//check for post-specific settings using ACF plugin options
					if(function_exists('get_field')) {
						$custom_activate = get_field('frn_rp_activate');
						if($custom_activate) {
							if(trim($custom_activate)!=="") {
								if(strtolower($custom_activate)=="yes") return TRUE; 
								elseif(strtolower($custom_activate)=="no") return FALSE;
							}
						}
					}

					$curr_type=get_post_type();
					if(!is_front_page() && !is_home() && $curr_type!=="attachment" && $curr_type!=="revision" && $curr_type!=="nav_menu_item" && !stripos(get_the_title(),"Contact") ) {

						/*
						NOTES:
						if post types selected
							use exclude IDs

						if post types not selected
							use include IDs
						*/

						$post_type_match=false;
						if(!isset($rp['post_types'])) $rp['post_types']="";
						if(!isset($rp['include'])) $is_included="";
							else $is_included=trim($rp['include']); //since we are testing it, need to clean it
						if(is_array($rp['post_types'])) {
							foreach($rp['post_types'] as $post_type) {
								if($post_type) {
									//echo "<h1>Current type: ".get_post_type()."; Selected Type: ".$post_type."</h1>";
									if(get_post_type()==$post_type) {
										$post_type_match=true;
									}
								}
							}
							if($post_type_match) {
								if(!isset($rp['exclude'])) $is_excluded="";
									else $is_excluded=trim($rp['exclude']); //since we are testing it, need to clean it
								if($is_excluded!=="") {
									$is_excluded=explode(",",$is_excluded);
									foreach($is_excluded as $excl_post){
										if(get_the_ID()==trim($excl_post)) return FALSE;
									}
								}
								return TRUE;
							}
							else {
								//if the current post type is not selected, check the include IDs array for the post ID.
								if($is_included!=="") {
									$is_included=explode(",",$is_included);
									foreach($is_included as $incl_post){
										if(get_the_ID()==trim($incl_post)) {
											$frn_related_act=TRUE;
											return TRUE;
										}
									}
								}
							}
						}
						else {
							//post_types is not an array, which means no post types selected
							//still check includes field if current post ID is in the array
							if($is_included!=="") {
								$is_included=explode(",",$is_included);
								foreach($is_included as $incl_post){
									if(get_the_ID()==trim($incl_post)) {
										$frn_related_act=TRUE;
										return TRUE;
									}
								}
							}
						}

						


					} //end if front page
				} //end if page/post
			} //rp activations
		}
	}
	return FALSE; //final stage: if not "return" prior to this, then it means the current post didn't meet any requirements

}






function frn_auto_rp_placement($content,$rp_block="") {
	//This function determines where the list will be placed in the content
	//This wouldn't even be called if a shortcode is in the content
	//It's purpose is to use a DOM approach helping to navigate around competing content
	//As of 7/24/17, the competing content is citations at the bottom of posts.
	//DEFAULT: RPs will be placed above citations

	/*
		Three methods:
			1. If ID provided, look for an object with that ID and place RPs above it (this is rarely used)
			2. If no ID, then try to use a word search for common citation section titles and attempt to move RPs at least above that title. (This won't always look good, but it's a step closer to what we want.)
			3. No citations found, just return content with list at bottom.
	*/


	/////
	// PREPARE DEFAULT STYLE SETTINGS
	$rp = get_option('site_rp');
	$citation_id=""; $citation_text = "";

	$class=""; $id=""; $styling=false;
	//Check and load current post custom fields for formatting related posts
 	//Expects ACF to be set up using these field vars
	if(function_exists('get_field')) {
		$styling = get_field('frn_rp_styling');
	}
	if($styling) {
		$class=trim($styling['class']);
		$id=trim($styling['id']);
	}

	if($id=="") {
		if(!isset($rp['id'])) $rp['id']="";
		if($rp['id']!=="") $id=$rp['id'];
		else $id="frn_related_posts"; //required because it removes the RP anchor from TOC
	}

	if($class=="") {
		if(!isset($rp['class'])) $rp['class']="";
		if($rp['class']!=="") $class=$rp['class'];
		else $class="frn_related_posts";
	}


	//////
	/// CITATION HEADERS TO LOOK FOR
	$citation_hdrs = array(
		"sources",
		"citations", //added 9/12/17 (R-I.org)
		"references",
		"bibliography" //added 1/31/18 (TalbottCampus.com)
		);
	$citation_hdrs_cnt = count($citation_hdrs);





	//////
	// ACTIVATE DOM

	$citation_id="";
	/*
	//This is a lighter method, but as of 8/2017 no one wanted to direct editors and those posting content to add DIV HTML with an ID. 
	//It was fully functional, but unnecessary to include. Since the effort was already done, keeping in case we change our minds in the future.
	if(!isset($rp['citations_id'])) $rp['citations_id']="";
	$rp['citations_id']= trim($rp['citations_id']);
	if($rp['citations_id']!=="") {
		if(stripos($content,$rp['citations_id'])) $citation_id = "found";
	}
	*/

	$citation_text=""; $i=0;
	while($citation_text!=="found" && $i<$citation_hdrs_cnt) {
		if(stripos($content,$citation_hdrs[$i])) $citation_text = "found";
		$i++;
	}

	if($citation_id=="found" || $citation_text=="found") {
		//citation_id not used
		//IMPORTANT: Keep in mind that in-context personalize boxes and external link processing also use DOM
		//maybe this could be turned into a function where DOMs are only activated on content once
		libxml_use_internal_errors(true);
		$dom = new DOMDocument(null, 'UTF-8');
		$dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ) );
		libxml_clear_errors(); //keeps DOM errors from appearing on page
	}



	/*
	//////////
	// OPTION #1: Look for citations block using specified ID

	//This is a lighter method, but as of 8/2017 no one wanted to direct editors and those posting content to add DIV HTML with an ID. 
	//It was fully functional, but unnecessary to include. Since the effort was already done, keeping in case we change our minds in the future.
	//If an ID is added to the main settings, we can activate our DOM approach
	if($citation_id=="found") {
		//Find citations section by ID
		$citations = $dom->getElementById($rp['citations_id']);
		if($citations) {
			//create the DIV wrapper for the RP list with custom ID and CLASS
			//Insert the wrapped list
			$list_domElement = $dom->createElement('div',$rp_block);
				$wrapper_id = $dom->createAttribute('id');
				$wrapper_id->value = $id;
				$list_domElement->appendChild($wrapper_id);
				$wrapper_class = $dom->createAttribute('class');
				$wrapper_class->value = $class;
				$list_domElement->appendChild($wrapper_class);
			$citations->parentNode->insertBefore($list_domElement, $citations);
			//I don't understand DOM enough, but this preg_replace is required for content to display correctly.
			return preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', htmlspecialchars_decode($dom->saveHTML()));
		}

	} 
	*/



	//////////
	//OPTION #2: if ID not found, look for text

	if($citation_text=="found") {
		/*
			NOTES:
			This feature only workes if the citations headers are the only thing in a block.
			This strips out all HTML and only looks at the text within each block. 
			So as long as the header is wrapped in tags like P, H2, H3, DIV, SPAN, I, etc, this function will find it.
			Versions of Headers;
			1. Sources
			2. Citations
			3. References
		*/

		$all_content = $dom->getElementsByTagName('*');
		//echo "<h1>Content returned by DB</h1>".$content."<h1>End of Content returned by DB</h1>";

		
		//echo "<h1>".$all_content->length." Blocks On The Page</h1>";


		////////
		/// CREATE THE DIV AND LIST

		$list_domElement = $dom->createElement('div',$rp_block);
			$wrapper_id = $dom->createAttribute('id');
			$wrapper_id->value = $id;
			$list_domElement->appendChild($wrapper_id);
			$wrapper_class = $dom->createAttribute('class');
			$wrapper_class->value = $class;
			$list_domElement->appendChild($wrapper_class);

		$clear_domElement = $dom->createElement('div','');
			$clear_class = $dom->createAttribute('class');
			$clear_class->value = 'frn_clear';
			$clear_domElement->appendChild($clear_class);

		$loop=0; $prev_loop_block="";
		foreach($all_content as $block){
			
			$inner_text=wp_strip_all_tags($block->nodeValue);
			$inner_text=trim(strtolower($inner_text));
			
			if($inner_text!=="") {
				if(!is_numeric($inner_text)) {
					if($prev_loop_block!==$inner_text) {
						//FOR TESTING: echo "<b style=\"color:red;\">".$loop."</b>";

						$citation_hdr=""; $i=0;
						while($citation_hdr=="" && $i<$citation_hdrs_cnt) {
							//Remember that the citation header needs to be the only thing within a block
							//For whatever block that the header is in, the RPs will be added directly above it
							//As a result, if you have the header within a DIV, the RPs will be added within that DIV and above the header.
							//If you have an <hr> above the sources' header, the RPs will be added between the <hr> and header.
							//etc.
							
							$prev_loop_block=$inner_text;
							//echo "<br /><br />[ <b style=\"color:red;\">Main #: ".$loop."; Sources #: ".($i+1)."</b> Looking for \"".$citation_hdrs[$i]."\"; Inner Text: ".$inner_text." ]";
							if($inner_text==$citation_hdrs[$i] || $inner_text==$citation_hdrs[$i].":") {
								//echo ": <b>Block Matched!</b> Stopping loop and returning content...<br /> <br />";
								$citation_hdr = $citation_hdrs[$i];
								$block->parentNode->insertBefore($list_domElement, $block );
								$block->parentNode->insertBefore($clear_domElement, $block );
								//FOR TESTING: echo "<b style=\"color:red;\">RPs go here</b>";
							}
							$i++;
						}
						$loop++;
					}
				}
			}

			//Once a header is found, no need to continue looping through block. Just return the saved HTML.
			if($citation_hdr!=="") { 
				//I don't understand DOM enough, but this preg_replace is required for content to display correctly.
				return preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', htmlspecialchars_decode($dom->saveHTML()));
			}

		}

		

	} 


	// OPTION #3: There are no citations on the page, so just add it to the end.
	//   By this point, if nothing has been returned, then we can just use our normal, easy approach

	$rp_block = "
		<div id=\"".$id."\" class=\"".$class."\">
			<div class=\"".$class."_inner\">"
				.$rp_block."
			</div>
		</div>
		<div class=\"frn_clear\"></div>
		";

	return $content.$rp_block;


	//////
	//  THIS CULMINATES THE RELATED POSTS FEATURE
	//////


}
?>