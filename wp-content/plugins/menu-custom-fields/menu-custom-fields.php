<?php 

/*
Plugin Name: Menu Custom Fields
Plugin URI: http://foxfuelcreative.com/
Description: A simple plugin that adds custom fields to menus
Version: 1.0
Author: Foxfuel Creative
Author URI: http://foxfuelcreative.com/
License: GPL2
*/

class menu_custom_fields {

	/*--------------------------------------------*
	 * Constructor
	 *--------------------------------------------*/

	/**
	 * Initializes the plugin by setting localization, filters, and administration functions.
	 */
	function __construct() {
		// add custom menu fields to menu
		add_filter( 'wp_setup_nav_menu_item', array( $this, 'mcf_add_custom_nav_fields' ) );// save menu custom fields
		add_action( 'wp_update_nav_menu_item', array( $this, 'mcf_update_custom_nav_fields'), 10, 3 );
		// edit menu walker
		add_filter( 'wp_edit_nav_menu_walker', array( $this, 'mcf_edit_walker'), 10, 2 );
	} // end constructor

	
	
	
	
	/* All functions will be placed here *//**
	 * Add custom fields to $item nav object
	 * in order to be used in custom Walker
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function mcf_add_custom_nav_fields( $menu_item ) {


		$menu_item->intro = get_post_meta( $menu_item->ID, '_menu_item_intro', true );
		$menu_item->image = get_post_meta( $menu_item->ID, '_menu_item_image', true );
		$menu_item->cta = get_post_meta( $menu_item->ID, '_menu_item_cta', true );
		return $menu_item;

	}
	
	
	
		/**
	 * Save menu custom fields
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function mcf_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {


		
		//intro content
		// Check if element is properly sent
		if ( is_array( $_REQUEST['menu-item-intro']) ) {
			$intro = $_REQUEST['menu-item-intro'][$menu_item_db_id];
			update_post_meta( $menu_item_db_id, '_menu_item_intro', $intro );
		}
		
		//image content
		// Check if element is properly sent
		if ( is_array( $_REQUEST['menu-item-image']) ) {
			$image = $_REQUEST['menu-item-image'][$menu_item_db_id];
			update_post_meta( $menu_item_db_id, '_menu_item_image', $image );
		}
		
		//cta content
		// Check if element is properly sent
		if ( is_array( $_REQUEST['menu-item-cta']) ) {
			$cta = $_REQUEST['menu-item-cta'][$menu_item_db_id];
			update_post_meta( $menu_item_db_id, '_menu_item_cta', $cta );
		}


	}

	//no longer used!
	
	/**
	 * Define new Walker edit
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function mcf_edit_walker($walker,$menu_id) {

		return 'Walker_Nav_Menu_Edit_Custom';

	}
}

include_once( 'edit_custom_walker.php' );
include_once( 'talbott_custom_menu_walker.php' );

// instantiate plugin's class
$GLOBALS['menu_custom_fields'] = new menu_custom_fields();

?>