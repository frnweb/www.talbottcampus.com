<?php 



// Menu output mods
class talbott_custom_menu_walker extends Walker_Nav_Menu{
	public $cta;
  function start_el(&$output, $object, $depth = 0, $args = Array(), $current_object_id = 0){

	 global $wp_query;
	 $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
	
	 $class_names = $value = '';
	 $li_attributes = '';
	
		// If the item has children, add the dropdown class for bootstrap
		if ( $args->has_children ) {
			$class_names = "has-kids ";
			$li_attributes .= ' data-dropdown-id="menu-shell-'. $object->ID . '"';
		}
	
		$classes = empty( $object->classes ) ? array() : (array) $object->classes;

		
		$class_names .= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $object ) );
		$class_names = ' class="'. esc_attr( $class_names ) . '"';
       
   	$output .= $indent . '<li id="menu-item-'. $object->ID . '"' . $value . $class_names . $li_attributes .'>';

   	$attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
   	$attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
   	$attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
   	$attributes .= ! empty( $object->url )        ? ' href="'   . esc_attr( $object->url        ) .'"' : '';

   	// if the item has children add these two attributes to the anchor tag
   	if ( $args->has_children ) {
		  $attributes .= ' class="hasDropdown" ';
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before .apply_filters( 'the_title', $object->title, $object->ID );

    $item_output .= $args->link_after;

    // if the item has children add the caret just before closing the anchor tag
    if ( $args->has_children ) {
    	$item_output .= '<i class="fa fa-sort-desc" aria-hidden="true"></i></a>';
    }
    else {
    	$item_output .= '</a>';
    }
	if($depth == 0){
		
	
		//used for the top level items
		$html = '<div class="menu-shell" id="menu-shell-'.$object->ID.'"> ';
			$html .= '<div class="row tb-pad-20" id="mainSubNavRow" data-equalizer data-equalize-on="large">';
				$html .= '<div class="large-5 large-push-3 columns" data-equalizer-watch>';
					$html .= '<div class="image-shell"><a href="'. $object->url .'"><img src="'.$object->image.'"/></a></div>';
				$html .= '</div>';
				$html .= '<div class="large-4 large-push-3 columns main-sub-nav-intro-col" data-equalizer-watch>';
					$html .= '<h1><a href="'. $object->url .'">'.$object->title.'</a></h1>';
					$html .= '<div class="intro"><p>'.$object->intro.'</p></div>';
				$html .= '</div>';
				$html .= '<div class="large-3 large-pull-9 columns last-sub-nav-col" data-equalizer-watch>';
			//closing div is put after the output ;
		
		$item_output .= $html;
		
		$item_output .= $args->after;
		
		if ( !empty($object->cta)){
			$this->cta = $object->cta;
		} else{
		 $this->cta = null;
		}		
	} else{
		$item_output .= $args->after;
	}

	
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $object, $depth, $args );
	 
	
  } // end start_el function
        
  function start_lvl(&$output, $depth = 0, $args = Array()) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"mm-subnav\">\n";
  }
      
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
    $id_field = $this->db_fields['id'];
    if ( is_object( $args[0] ) ) {
        $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
    }
    return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
  }   




	/**
	 * Ends the element output, if needed.
	 *
	 * @since 3.0.0
	 *
	 * @see Walker::end_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Page data object. Not used.
	 * @param int    $depth  Depth of page. Not Used.
	 * @param array  $args   An array of wp_nav_menu() arguments.
	 */
	public function end_el( &$output, $object, $depth = 0, $args = array() ) {
		if($depth == 0 && $args->has_children){
			$output .= "</div>\n";
			$output .= "</div>\n";
			$output .= "</div>\n";
			$output .= "</li>\n";
		}else{
			$output .= "</li>\n";
		}
		
	}
	
	
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
		if ( !empty($this->cta)){
			$output .= '<div class="cta">'.$this->cta.'</div>';
		} 		
			
			
	}
  
}




?>