


Handlebars.registerHelper('ifCond', function(v1, v2, options) {
  if(v1 === v2) {
    return options.fn(this);
  }
  return options.inverse(this);
});
    var assessment_template = Handlebars.compile( $('#assessment-template').html() );
    var result_template = Handlebars.compile( $('#result-template').html() );
    Handlebars.registerPartial( 'question', $('#question-template').html() );
jQuery(document).ready(function( $ ) {

    
    

    
    //get the assessment data
    var domain = window.location.hostname;
    var url = document.location.protocol+'//'+domain+'/wp-json/wp/v2/assess_ques?filter[orderby]=menu_order&filter[order]=ASC';
    var ques;
    
    $.get(url, {}, function(data){
        ques = data;
        var html = assessment_template({questions:data});
        //console.log(html);
        $('#assessment-shell').html(html);
			
       initAssessment();
		 //setTimeout(function(){ $(window).resize(); }, 300);
    });
    $('#assessment-shell').on('click', '.question button.answer', function(){
       var answer = $(this).data('answer');
       // console.log(answer);
        var index = $(this).data('index');
       // console.log(ques[index]);
        
         $('#assessment').find('.question_'+index+' .message').html('');
        ques[index].answer = answer;
        //are we at the end? or a normal slide?
        //console.log( ques.length );
        //console.log(index);
        if(index ==  (ques.length -1)){
            processAssessment(ques);
        } else{
            $('#assessment .questions').cycle('next');
        }
        
    });
    $('#assessment-shell').on('click', '.question button.nav', function(){
        var action = $(this).data('action');
       
        var index = $(this).data('index');
         $('#assessment').find('.question_'+index+' .message').html('');
        if(action == 'next'){
            //did the user actually answer the question?
            if( ques[index].answer !== undefined ){
                $('#assessment .questions').cycle('next');
            } else{
                $('#assessment .questions').find('.question_'+index+' .message').html('Please answer the question to continue.');
            }
            
        }
        if(action == 'back'){
            $('#assessment .questions').cycle('prev');
        }
        
    });
    $('#assessment-shell').on( 'cycle-before', '.questions', function (event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
        //console.log(incomingSlideEl);
        //if we are on the last slide, we just bump the assessment to 100%
        if ( $(outgoingSlideEl).data('index') == (ques.length - 1) ){
            //console.log('last');
        } 
        
        calculateProgress(incomingSlideEl, ques);
    });  
    $('#assessment-shell').on( 'cycle-after', '.questions', function (event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {

		slider_fix();

    });
	
	$(window).scroll(function(){
		assessArrow();
	});
	$(window).resize(function(){
		assessArrow();
	});
	
});

var assessArrowFired = false;
function assessArrow() {
	if($(window).width() > 640 && assessArrowFired == false) {
		if($('#assessment-shell').offset().top <= ($(window).scrollTop() + ($(window).outerHeight()*0.75))) {
			var aA_Offset = ($('#home-contact-form').height()-$('#home-contact-form input[type="submit"]').outerHeight())-20;
			var aA_max_offset = $('#assessment-shell').outerHeight() - (72);
			if(aA_Offset < aA_max_offset && aA_Offset > 20) {
				$('#assess-arrow-shell').animate({top:aA_Offset},{duration: 3000});
				assessArrowFired = true;
			} else {
				$('#assess-arrow-shell').css('top', 20);
			}
		}
	}
}

 function calculateProgress(incomingSlideEl, ques){
        var length = ques.length;
        var index = $(incomingSlideEl).data('index');
        var percent = (index / length)*100;
     //console.log(index);
     //console.log(length);
        percent = Math.floor(percent);
        $('.progress-meter').animate({
            width:percent+'%'
        }, 400);
       
 }
    function initAssessment(){
        
		  $('#assessment .questions').cycle({
            timeout: 0,
            manualSpeed: 500,
            slides:'.question', 
            fx:'scrollHorz', 
            autoHeight:'calc',
            allowWrap: false
        });
		  //slider_fix($('#assess-progress-bar').outerHeight());
		 // setTimeout(function(){ slider_fix( $('#assess-progress-bar').outerHeight() ); }, 300);
		  
    }
    function processAssessment(ques){
       // console.log( ques );
        var threshold = (3/7);
        var factors = 0;
        var assessment_data = {};
        assessment_data.questions = [];
        var body = '';
        var k = 0;
        for( var i = 0; i<ques.length; i++){
            k = 1+i;
            
            var question = {};
            body +="Question "+k+": "+ques[i].title.rendered+"\r\n";
            body +="Answer "+k+": "+ques[i].answer+"\r\n";
            body +="\r\n";
            question.question = ques[i].title.rendered;
            question.answer = ques[i].answer;
            assessment_data.questions.push(question);
            if(ques[i].answer=='yes'){
                factors = factors + 1;
            }
        }
        //calculate percentage
        var percent = factors / ques.length;
        //are we at risk?
        //generate a response
        var response = {};
        response.total = ques.length;
        response.factors = factors;
        if (percent > threshold){
			//more risk
            response.message ='You show <span class="numbers">'+response.factors+' of '+response.total+'</span> factors of risk. If you would like to have a conversation about early indicators of a substance abuse problem, contact us today. Our coordinators can help you understand the symptoms of mild and severe behavioral health issues and what you can do to stay healthy. Please fill out the contact form on this page to be connected to a coordinator.';
        } else {
			//less ris;
            response.message ='You show <span class="numbers">'+response.factors+' of '+response.total+'</span> factors of risk. One of our admissions coordinators can ask you more specific questions and give you a better idea of what your options are. Speaking to one of our coordinators does not involve any type of obligation, and a detailed over-the-phone assessment is conducted at no cost to you. Please fill out the contact form on this page to be connected to a coordinator.';
        }
        //console.log(factors);
        //response.index = response.total + 1;
        var html = result_template(response);
         $('#assessment .questions').cycle('add', html);
        $('#assessment .questions').cycle('next');
        //store their results with the contact form
        //console.log(assessment_data);
        var string = JSON.stringify(assessment_data);
        //console.log(body);
        $('#assessment_data').attr('value',body );
    }
	 function slider_fix(add){
		 
		var add = add || 0;
		var slide_h = 0;
		var h = 0;
		$('#assessment-shell .cycle-slide').each(function(){
			h = $(this).outerHeight();
			//var bh = $(this).find('.buttons').outerHeight();
			//h += bh;
			if( !$(this).hasClass('.cycle-sentinel') ){
				if(h > slide_h){
					slide_h = h;
				}			
			}
			//console.log('slide_h: '+slide_h);

		});
		var new_height = slide_h + add + $('#assess-progress-bar').outerHeight();
		//console.log(new_height);
		$('#assessment-shell .questions').css({'min-height':new_height+'px'});
		//$(assessment_selector).css({'min-height':slide_h+'px'});
		

	 }
    

