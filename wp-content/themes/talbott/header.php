<!doctype html>  

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html <?php language_attributes(); ?> class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title( '|', true, 'right' ); ?></title>	
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
  		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-57x57.png?ver=1.2">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-60x60.png?ver=1.2">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-72x72.png?ver=1.2">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-76x76.png?ver=1.2">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-114x114.png?ver=1.2">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-120x120.png?ver=1.2">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-144x144.png?ver=1.2">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-152x152.png?ver=1.2">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-180x180.png?ver=1.2">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri()  ?>/images/favicon/android-icon-192x192.png?ver=1.2">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri()  ?>/images/favicon/favicon-32x32.png?ver=1.2">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri()  ?>/images/favicon/favicon-96x96.png?ver=1.2">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri()  ?>/images/favicon/favicon-16x16.png?ver=1.2">
		<link rel="manifest" href="<?php echo get_template_directory_uri()  ?>/images/favicon/manifest.json?ver=1.2">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri()  ?>/images/favicon/ms-icon-144x144.png?ver=1.2">
		<meta name="theme-color" content="#ffffff">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,600,700,600italic,700italic|Droid+Serif:700,700i,400' rel='stylesheet' type='text/css'>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-TML9TV');</script>
		<!-- End Google Tag Manager -->

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()  ?>/plugins/Remodal-1.1.0/dist/remodal.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()  ?>/plugins/Remodal-1.1.0/dist/remodal-default-theme.css" />
		
		<!-- custom css for foundatiosn team -->
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()  ?>/css/custom.css" />
		
		<!-- end of wordpress head -->
		<!-- IE8 fallback moved below head to work properly. Added respond as well. Tested to work. -->
			<!-- media-queries.js (fallback) -->
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>			
		<![endif]-->

		<!-- html5.js -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->	
		
			<!-- respond.js -->
		<!--[if lt IE 9]>
		          <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
		<![endif]-->	
		<?php if ( is_front_page() ): ?>
			<script type='text/javascript' src="<?php echo get_template_directory_uri()  ?>/library/js/magnific.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()  ?>/css/magnific.css" />
		<?php endif;  
		/*
			// commented this out 8/22/17
			// Dax was not sure which account this was for. Causing problems with bounce rates being 0%. Removing but keeping in case someone contacts us who needed the information.
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-56397762-20', 'auto');
			  ga('send', 'pageview');

			</script>
		*/
		?>
	</head>
	
	<body <?php body_class(); ?>>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TML9TV"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div id="top-search">
			<div id="top-search-form-box">
				<a href="#close" id="top-search-close"></a>
				<p>Start typing to search</p>
				<form action="<?php echo home_url( '/' ); ?>" method="get" class="form-inline search-form">
					<div class="input-group">
						<span class="input-group-label show-for-lt-ie9">Search</span>
						<input class="input-group-field form-control" name="s" id="search" placeholder="" value="" type="text">
						<!--<button type="submit" class="button"><?php _e("Go","wpbootstrap"); ?></button>-->
					</div>
				</form>
			</div>
		</div>
		<div id="mobile-nav-overlay"></div>
		<header id="site-header">
			<div class="row">
				<div class="medium-12 columns">
					<!--<a href="tel:<?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks [Global Header"]'); ?>">-->
						
					<!--</a>-->
					<!--<span id="frn_phones" ga_phone_location="Phone Clicks in Global Header" frn_number="[frn_phone only='yes']"> </span>-->
					<div id="miniNavPhoneBlock">
						<span id="header-phone" > <?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Global Header"]');?> </span>
						<span id="secondary-nav-links">
							
							 <?php secondary_nav(); ?>
						</span>
					</div>
					<a href="<?php echo site_url(); ?>" id="site-logo-link">
						<img src="<?php echo get_template_directory_uri()  ?>/images/talbott-recovery-alcohol-treatment.png" alt="Talbott Recovery" class="header-logo-gray">
					</a>
					<div class="clearfix"></div>
				</div>
			</div>
		</header>
		<div class="clearfix"></div>
		<div id="navStopper">
			<div data-sticky-container>
				<div class="sticky" data-sticky data-sticky-on="small" data-options="marginTop:0;" data-anchor="navStopper" style="width: 100%;">
					<div id="main-nav">
						<div id="large-nav">
							<div class="row">
								<div class="large-12 columns">
									<ul class="menu logo-menu">
										<li class=""><div><a href="<?php echo site_url(); ?>"><div id="nav-logo" class="hide-logo"></div></a></div></li>
									</ul>
									<span id="primary-nav-links">
										 
										 <?php wp_bootstrap_main_nav(); ?>
									</span>

									
									<!--<a href="." id="mega-btn" class=""></a>-->
									<ul class="menu phone-menu float-right">
										<li class="phone-menu-number"><div><?php echo do_shortcode('[frn_phone]');?></div></li>
										<li class=""><a id="top-nav-search-btn" href="#" class="open-top-search"><div id="top-nav-search-icon"></div></a></li>
										<li id="top-nav-mobile-menu-li"><a id="top-nav-mobile-menu-link" href="#" class="mobile-nav-closed"><span id="mobile-nav-text-closed">Menu</span><span id="mobile-nav-text-opened">Close</span></a></li>
									</ul>
								</div>
							</div>
						</div>
						<div id="mobile-nav-links">
							<?php mobile_nav(); ?>
							<div id="mobile-secondary-nav-links">
								<?php secondary_nav(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>				
			<div id="global-side-nav" class="expanded">
				<?php 
			
				
				?>
				<div id="gsn-links">
					<ul>
						<?php if(is_chat_live()): ?>
							<li class="gsn-link-3">
								<?php echo do_shortcode('[lhn_inpage button="chat" class="gsn-square-link gsn-chat" category="Global Contact Options" where_on_page="Chat Toggles" title="Chat with us" text="empty" offline="remove" ]'); ?>
							</li>
							<li class="gsn-link-2">
								<?php echo do_shortcode('[lhn_inpage button="email" class="gsn-square-link gsn-email" category="Global Contact Options" where_on_page="Email Toggles" title="Email us" text="empty" ]'); ?>
							</li>
						<?php else: ?>
							<li class="gsn-link-2">
								<?php echo do_shortcode('[lhn_inpage button="email" class="gsn-square-link gsn-email no-chat" category="Global Contact Options" where_on_page="Email Toggles" title="Email us" text="empty" ]'); ?>
							</li>
						<?php endif; ?>
						<li class="gsn-link-1">
							<?php echo do_shortcode('[frn_phone id="snPhone" class="gsn-square-link gsn-call" title="Call us" text="empty" ga_phone_category="Global Contact Options" ga_phone_location="Custom Buttons: Phone Touches" desktop_url="/contact/" desktop_action="Desktop Clicks to Contact Us"]'); ?>
							<?php /*
								//the following is the old code used. New code automatically detects the type of device and changes links and text.
								if( is_mobile() ):?>
								<?php //echo do_shortcode('[frn_phone id="snPhone" class="gsn-square-link gsn-call" title="Call us" text="empty" ga_phone_category="Global Contact Options" ga_phone_location="Custom Buttons: Phone Touches"]'); ?>
							<?php else: ?>
								<!--<a href="/contact/"  id="" class="gsn-square-link gsn-call" title=""></a>-->
							<?php endif; 
							*/ ?>
						
						</li>
					</ul>
				</div>
				<div id="gsn-button-box">
					<a href="#contact" id="gsn-main-button" class="gsn-square-link">Contact</a>
				</div>
			</div>
			<div id="content">