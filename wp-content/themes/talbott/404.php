<?php 


///////
//Prepare the keyword search by pulling words from the URL

//$s = str_replace(get_site_url(),"",$wp_query->query_vars['name']);  //keeping here just in case
$s = str_replace("/"," ",$_SERVER['REQUEST_URI']);
//$s = trim(preg_replace("/(.*)-html|htm|php|asp|aspx)$/","$1",$s));


	$s = urldecode($s);
	$s = trim(str_replace("-"," ",$s));
	$s = strtolower(preg_replace('/[0-9]+/', '', $s )); //remove all numbers
	$stop_words = array(
		"for",
		"the",
		"and",
		"an",
		"a",
		"is",
		"are",
		"than",
		"that",
		"I",
		"to",
		"on",
		"it",
		"with",
		"can",
		"be",
		"of",
		"get",
		"in",
		"you",
		"from",
		"if",
		"by",
		"so",
		"at",
		"do",
		"&",
		"there",
		"too"
	);
	$i=1;
	foreach($stop_words as $word){
		/*
		//for testing:
		if($i==1) {
			echo $s."<br />";
			echo $word."<br />";
			echo "string: ".strlen($s);
			echo "; word: ".strlen(" ".$word)."<br />";
			echo "position: ".strpos($s,$word." ")."<br />";
			echo "string without word: ".(strlen($s)-strlen(" ".$word))."<br />";
		}
		*/
		$word = trim(strtolower($word));
		$s = str_replace(" ".$word." "," ",$s); ///in the middle
		if(strpos($s,$word." ")===0) $s = str_replace($word." ","",$s); // at the beginning
		if(strpos($s," ".$word)==strlen($s)-strlen(" ".$word)) $s = str_replace(" ".$word,"",$s); // at the end
		$i++;
	}
	




	///////
	//Prepare the list of search results

	//future option: checking if only one page returned, then immediately forwarding the person to that page instead
	//check if relevanssi plugin is activated
	if (function_exists('relevanssi_do_query')) {
		$url_terms_search = new WP_Query();
		$url_terms_search->query_vars['s']				=$s;
		$url_terms_search->query_vars['posts_per_page']	=8;
		$url_terms_search->query_vars['paged']			=0;
		$url_terms_search->query_vars['post_status']	='publish';
		relevanssi_do_query($url_terms_search);
     }
     else {
	    //global $wpdb;
		$url_terms_search = new WP_Query( array( 
			's' => 'treatment', 
			//'page_id' => 26,
			'post_type' => 'any', //array( 'post', 'page' ),
			'posts_per_page' => 8,
			'post_status' => 'publish'
		));
     }




get_header(); ?>
			
			<div id="content" class="clearfix row tb-pad-40">
			
				<div id="main" class="small-12 columns" role="main">

					<article id="post-not-found" class="clearfix">
						
						<header>

							<div class="hero-unit">
							
								<h1><?php _e("Error 404 - Page Not Found","wpbootstrap"); ?></h1>
								<p><?php _e("This is embarassing. We can't find the page you were looking for.","wpbootstrap"); ?></p>
															
							</div>
													
						</header> <!-- end article header -->
					
						<section class="post_content">
							
							<?php
				                if ( isset($url_terms_search) ) {
				                if ( $url_terms_search->have_posts() ) { ?>
								<p><b>Were you looking for one of these?</b></p>
								<?php 
								echo "
								<!-- Words used in search: \"".$s."\" -->
								";
								?>
								<ul class="frn_suggestions">
								<?php
								while ( $url_terms_search->have_posts() ) {
									$url_terms_search->the_post();
									?>
									<li>
										<a href="<?php the_permalink();?>"><?php the_title();?></a>
									</li>
									<?php 
								}
								?>
								</ul>
								<br />
								<?php
								} else { ?>
									<p>
										And unfortunately, we can't find any posts that relate to that web address. 
										Take a look at it in the address bar above and see if it looks pretty normal. 
										Make corrections if not and try again.
									</p>
								<?php }
							}
				                
				            ?>


							<p><?php _e("...or maybe try searching.","wpbootstrap"); ?></p>

							<div class="row">
								<div class="small-12 columns">
									<?php get_search_form(); ?>
								</div>
							</div>
					
						</section> <!-- end article section -->
						
						<footer>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>