				<div id="call-us">
					<div class="row tb-pad-60">
						<div class="large-12 columns text-center">
							<img src="<?php echo get_template_directory_uri()  ?>/images/icon.phone.png" alt="" style="width: 40px; margin-bottom: 30px;">
							<h2 id="footer-phone">Call us now at <?php echo do_shortcode('[frn_phone]');?></h2>
							<p id="call-us-or-find">Or find us at</p>
							<div id="call-us-address"><?php echo get_field('company_address', 'option'); ?></div>
						</div>
					</div>
				</div>
			</div><!-- #content -->
		</div><!-- nav sticky -->
		<footer class="tb-pad-60">
			<div class="row">
				<div class="large-12 columns">
					<div class="text-center">
						<div>
							<?php wp_bootstrap_footer_links(); // Adjust using Menus in Wordpress Admin ?>
						</div>
						<div>
							<ul class="menu">
								<li><a href="<?php the_field('twitter_url', 'option') ?>" target="_blank" class="social twitter"></a></li>
								<li><a href="<?php the_field('facebook_url', 'option') ?>"  target="_blank" class="social facebook"></a></li>
								<li><a href="<?php the_field('youtube_url', 'option') ?>"  target="_blank" class="social youtube"></a></li>
							</ul>
						</div>
						
						<?php echo do_shortcode('[frn_footer]');?>
					</div>
				</div>
			</div>
		</footer>
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		
		<?php wp_footer(); // js scripts are inserted using this function ?>

		<script src="<?php echo get_template_directory_uri()  ?>/js/app.js"></script>
		<?php if ( is_front_page() ): ?>
			<script src="<?php echo get_template_directory_uri()  ?>/library/js/fancybox/source/jquery.fancybox.js"></script>
			<link type="text/css" href="<?php echo get_template_directory_uri()  ?>/library/js/fancybox/source/jquery.fancybox.css" rel="stylesheet"></link>
			<script src="<?php echo get_template_directory_uri()  ?>/library/js/fancybox/source/helpers/jquery.fancybox-media.js"></script>
			<script src="<?php echo get_template_directory_uri()  ?>/library/js/skywood-homepage.js"></script>

		<?php endif; ?>
	<?php if(  is_front_page()):?>
<script id="assessment-template" type="text/x-handlebars-template">
	<div id="assessment" class="">
		<div class="questions clearfix">
		{{#each questions }}
			<div class="question question_{{ @index }}" data-index="{{ @index }}">
				<div class="title">
					{{{ this.title.rendered }}}
				</div>
				<div class="buttons">
					<div class="row tb-pad-20">
						<div class="small-6 medium-5 columns">
							<button type="button" class="button large expanded  answer" data-answer="yes" data-index="{{ @index }}">
								Yes
							</button>
						</div>
						<div class="small-6 medium-5 columns">
							<button type="button" class="button large expanded  answer" data-answer="no" data-index="{{ @index }}">
								No
							</button>
						</div>
						<div class="medium-2 columns show-for-medium"></div>
					</div>
				</div>
				<div class="message">
				</div>
				<div class="nav-buttons hide">
					{{#if @first }}
						
					{{else}}
						<button type="button" class="button nav" data-action="back" data-index="{{ @index }}">
							Back
						</button>						
					{{/if}}
					{{#if @last }}
						
					{{else}}
						<button type="button" class="button nav" data-action="next" data-index="{{ @index }}">
							Next
						</button>	
					{{/if}}
				</div>
				<div class="clearfix"></div>
			</div>

		{{/each}}
		</div><!-- questions -->
		<div style="clear:both"></div>
		<div class="row" id="assess-progress-bar">
			<div class="medium-4 large-3 columns show-for-medium">
				<p style="margin-bottom: 0; line-height: .8;">Your Progress</p>
			</div>
			<div class="medium-8 large-9 columns">
				<div class="secondary progress">
					<div class="progress-meter"></div>
				</div>
			</div>
			
		</div>
		<div class="clearfix"></div>
		<div id="assess-arrow-shell" class="show-for-medium">
			<div id="assess-arrow-outer"></div>
			<div id="assess-arrow-inner"></div>
		</div>
	</div>
</script>
<script id="question-template" type="text/x-handlebars-template">
	<div class="question ">
			<h3>
			
			</h3>
	</div>
</script>
<script id="result-template" type="text/x-handlebars-template">
	<div class="question" data-index="{{ total }}">
		<div class="title">Results</div>
		<div class="">
			<p>{{{message}}}</p>
			<p id="assess-phone"><?php echo do_shortcode('[frn_phone]');?></p>
		</div>
	</div>
</script>
		<script src="<?php echo get_template_directory_uri()  ?>/library/js/handlebars.min.js"></script>
		<script src="<?php echo get_template_directory_uri()  ?>/library/js/jquery.cycle2.js"></script>
		<script src="<?php echo get_template_directory_uri()  ?>/library/js/assessments.js"></script>
	<?php endif; ?>

	</body>
</html>