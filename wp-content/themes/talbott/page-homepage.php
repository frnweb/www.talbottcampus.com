<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>
			
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php 
				$post_thumbnail_id = get_post_thumbnail_id();
				$featured_src = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
				$featured_mobile_src = get_field( 'mobile_featured_image');
				
				$curHPPostID = get_the_ID();
				
				
				
				$videoImage = get_field('placeholder_image_for_video');
				$videoImage = (isset($videoImage['url'])) ? $videoImage['url'] : '' ;
				
				$mobileVideoImage = get_field('mobile_placeholder_image_for_video');
				$mobileVideoImage = (isset($mobileVideoImage['url'])) ? $mobileVideoImage['url'] : $videoImage ;
				
				$videoURL = get_field('video_url');
			?>
			<div id="home-banner-outer">
				<div id="home-banner" style="background-image: url('<?php echo $videoImage; ?>');">
					<div id="home-banner-content">
						<div class="row tb-pad-60">
							<div class="large-8 medium-10 columns medium-centered">
								<div id="home-banner-play-btn" class="tb-pad-60">
									<a href="<?php echo $videoURL?>" class="button large" style="border-radius: 50%;"><i class="fa fa-play" aria-hidden="true"></i></a>
								</div>
								<h1><?php echo get_field('title'); ?></h1>
								<p><?php echo get_field('subtitle'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="home-banner-mobile" class="" style="background-image: url('<?php echo $mobileVideoImage; ?>');">
				<div>
					<div id="m-home-banner-content">
						<div class="row tb-pad-20">
							<div class="large-8 medium-10 columns medium-centered">
								<div id="m-home-banner-play-btn" class="tb-pad-30">
									<a href="<?php echo $videoURL?>" class="button large" style="border-radius: 50%;"><i class="fa fa-play" aria-hidden="true"></i></a>
								</div>
								<h1><?php echo get_field('title'); ?></h1>
								<p><?php echo get_field('subtitle'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
				<div id="our-treatment-programs">
					<div class="row tb-pad-60 text-center">
						<div class="large-8 medium-10 medium-centered columns">
							<h1><?php echo get_field('white_title'); ?></h1>
							<p><?php echo get_field('white_description'); ?><p>
						</div>
					</div>
				</div>
					
					<div id="services-section">
						<div class="row expanded columns medium-up-3">
							<div class="column text-center">
								<h3><?php echo get_field('service_icon_one_title', $curHPPostID); ?></h3>
								<?php $imgData = get_field('service_icon_one_image', $curHPPostID); ?>
								<div class="content-icon-box">
									<img src="<?php echo $imgData['url'];?>" alt="">
								</div>
								<a href="<?php echo get_field('service_icon_one_button_url', $curHPPostID); ?>" class="button hollow secondary"><?php echo get_field('service_icon_one_button_text', $curHPPostID); ?></a>
							</div>
							<div class="column text-center">
								<h3><?php echo get_field('service_icon_two_title', $curHPPostID); ?></h3>
								<?php $imgData = get_field('service_icon_two_image', $curHPPostID); ?>
								<div class="content-icon-box">
									<img src="<?php echo $imgData['url'];?>" alt="">
								</div>
								<a href="<?php echo get_field('service_icon_two_button_url', $curHPPostID); ?>" class="button hollow secondary"><?php echo get_field('service_icon_two_button_text', $curHPPostID); ?></a>
							</div>
							<div class="column text-center">
								<h3><?php echo get_field('service_icon_three_title', $curHPPostID); ?></h3>
								<?php $imgData = get_field('service_icon_three_image', $curHPPostID); ?>
								<div class="content-icon-box">
									<img src="<?php echo $imgData['url'];?>" alt="">
								</div>
								<a href="<?php echo get_field('service_icon_three_button_url', $curHPPostID); ?>" class="button hollow secondary"><?php echo get_field('service_icon_three_button_text', $curHPPostID); ?></a>
							</div>
						</div>
					</div>
					
					
					
					<?php 
					
					//video testimonialtype
					$args = array(
						'post_type'=>'testimonial',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$testimonials = new WP_Query( $args );
					if($testimonials->have_posts()) : 
						?>
												<?php 
												while($testimonials->have_posts()) : 
													$testimonials->the_post();	
													$title = get_the_title();
													$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
													$speaker = get_field('speaker');
?>
						<div id="video-testimonials" style="background-image:url(<?php echo $feat_image?>)">
							<div class="row tb-pad-60">
								<div class="small-12 columns">
									<div class="row">
										<div class="large-12 medium-12 medium-centered columns">
											<div class="row tb-pad-20 small-up-1">

													<div class="column">
														<div class="quote">
															<blockquote>
																<?php the_content(); ?>
															</blockquote>
														</div>
														<div class="speaker">
															<?php echo $speaker; ?>
														</div>
													</div>

											</div><!-- end row -->
										</div>
									</div>
								</div>
							</div>
						</div>
								<?php


							endwhile;
							?>						
						<?php 
					endif;
					
					?>
					
					
					
					<?php 
					//treatment content type
					$args = array(
						'post_type'=>'location',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$treatments = new WP_Query( $args );
					if($treatments->have_posts()) : 
						?>
						<div id="treatment-section">
							<div class="">
								<div class="col-sm-12">
									<?php 
									$treatment_count = 0;
									while($treatments->have_posts()) : 
										$treatment_count++;
										$treatments->the_post();	
										$title = get_the_title();
										
										$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
										?>
										<div class="row expanded collapse" data-equalizer data-equalize-on="medium">
											<div class="medium-6 columns <?php echo ($treatment_count %2 == 0) ? 'float-right' : '' ; ?>" >
												<div class="treatment-feature-image-box" data-equalizer-watch>
													<div class="treatment-feature-image" style="background: url('<?php echo $feat_image ?>') no-repeat center center / cover; transition: all 2.5s ease; -moz-transition: all 2.5s ease; -ms-transition: all 2.5s ease; -webkit-transition: all 2.5s ease; -o-transition: all 2.5s ease;" data-equalizer-watch>
												</div>
												</div>
											</div>
											<div class="medium-6 columns <?php echo ($treatment_count %2 == 0) ? 'float-left text-right' : '' ; ?>" data-equalizer-watch>
												<div class="treatment-pad">
													<div class="large-8 columns <?php echo ($treatment_count %2 == 0) ? 'float-right' : '' ; ?>">
														<div class="title">
															<h3><?php echo $title; ?></h3>
														</div>
														<div class="content">
															<?php if(get_field('intro_content')): ?>
																<p><?php echo get_field('intro_content'); ?><p>
															<?php else: ?>
																<?php the_excerpt() ?>
															<?php endif; ?>
														</div>
														<div class="button-shell">
															<?php 
																
																?>
							
															<a  class="button hollow" href="<?php the_permalink() ?>">
																Learn More
															</a>
																<?php 
																
															?>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										<?php
										?>
										</div><!-- end row -->
									<?php
									endwhile;
									?>
								</div>
							</div>
							
						</div>
						
						<?php wp_reset_postdata(); ?>


						<?php 
					endif;

					
					//insurance content type
					$args = array(
						'post_type'=>'insurance',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$insurances = new WP_Query( $args );
					if($insurances->have_posts()) : 
						?>
						<div id="insurance-section" class="tb-pad-90">
							<div class="row">
								<div class="small-12 columns">
									<h1 class="white-color text-center">
										We Accept These Insurances and More
									</h1>
								</div>
							</div>
							<div class="tb-pad-20">
								<div class="row small-up-2 medium-up-2 large-up-4">
									<?php 
									while($insurances->have_posts()) : 
										$insurances->the_post();	
										$title = get_the_title();
										$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
										?>
										<div class="column" style="padding-bottom: 20px;">
											<img class="insurance-logo" alt="<?php echo $title; ?>" src="<?php echo $feat_image ?>"/>
										</div>
										<?php
										
										
									endwhile;
									?>
								</div><!-- end row -->
							</div>
							<div class="row">
								<div class="large-12 columns text-center"><a href="<?php echo site_url(); ?>/admissions/the-abcs-of-insurance-coverage/" class="button hollow">Learn More</a></div>
							</div>
						</div>

<?php wp_reset_postdata(); ?>


						<?php 
					endif;
					?>
					
					
					

    
			
			<?php endwhile; ?>	
					
					
			<?php endif; ?>

<?php get_footer(); ?>