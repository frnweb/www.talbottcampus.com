<?php

// Add Translation Option
load_theme_textdomain( 'wpbootstrap', TEMPLATEPATH.'/languages' );
$locale = get_locale();
$locale_file = TEMPLATEPATH . "/languages/$locale.php";
if ( is_readable( $locale_file ) ) require_once( $locale_file );

// Clean up the WordPress Head
if( !function_exists( "wp_bootstrap_head_cleanup" ) ) {  
  function wp_bootstrap_head_cleanup() {
    // remove header links
    remove_action( 'wp_head', 'feed_links_extra', 3 );                    // Category Feeds
    remove_action( 'wp_head', 'feed_links', 2 );                          // Post and Comment Feeds
    remove_action( 'wp_head', 'rsd_link' );                               // EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' );                       // Windows Live Writer
    remove_action( 'wp_head', 'index_rel_link' );                         // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );            // previous link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );             // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); // Links for Adjacent Posts
    remove_action( 'wp_head', 'wp_generator' );                           // WP version
  }
}
// Launch operation cleanup
add_action( 'init', 'wp_bootstrap_head_cleanup' );

// remove WP version from RSS
if( !function_exists( "wp_bootstrap_rss_version" ) ) {  
  function wp_bootstrap_rss_version() { return ''; }
}
add_filter( 'the_generator', 'wp_bootstrap_rss_version' );

// Remove the […] in a Read More link
if( !function_exists( "wp_bootstrap_excerpt_more" ) ) {  
  function wp_bootstrap_excerpt_more( $more ) {
    global $post;
   // return '...  <div style="margin-top: 20px;"><a href="'. get_permalink($post->ID) . '" class="more-link button secondary" title="Read '.get_the_title($post->ID).'">Read more &raquo;</a></div>';
  }
}
add_filter('excerpt_more', 'wp_bootstrap_excerpt_more');

// Add WP 3+ Functions & Theme Support
if( !function_exists( "wp_bootstrap_theme_support" ) ) {  
  function wp_bootstrap_theme_support() {
    add_theme_support( 'post-thumbnails' );      // wp thumbnails (sizes handled in functions.php)
    set_post_thumbnail_size( 125, 125, true );   // default thumb size
    add_theme_support( 'custom-background' );  // wp custom background
    add_theme_support( 'automatic-feed-links' ); // rss

    // Add post format support - if these are not needed, comment them out
    add_theme_support( 'post-formats',      // post formats
      array( 
        'aside',   // title less blurb
        'gallery', // gallery of images
        'link',    // quick link to other site
        'image',   // an image
        'quote',   // a quick quote
        'status',  // a Facebook like status update
        'video',   // video 
        'audio',   // audio
        'chat'     // chat transcript 
      )
    );  

    add_theme_support( 'menus' );            // wp menus
    
    register_nav_menus(                      // wp3+ menus
      array( 
        'main_nav' => 'The Main Menu',   // main nav in header
        'footer_links' => 'Footer Links', // secondary nav in footer
    'secondary_nav' => 'Secondary (Tiny) Menu',   // expanded menu
      )
    );  
  }
}
// launching this stuff after theme setup
add_action( 'after_setup_theme','wp_bootstrap_theme_support' );

function wp_bootstrap_main_nav() {
  // Display the WordPress menu if available
  wp_nav_menu( 
    array( 
      'menu' => 'main_nav', /* menu name */
      'menu_class' => 'menu',
      'theme_location' => 'main_nav', /* where in the theme it's assigned */
      'container' => 'false', /* container class */
      'fallback_cb' => 'wp_bootstrap_main_nav_fallback', /* menu fallback */
      'walker' => new talbott_custom_menu_walker()
    )
  );
}

function wp_bootstrap_footer_links() { 
  // Display the WordPress menu if available
  wp_nav_menu(
    array(
      'menu' => 'footer_links', /* menu name */
      'theme_location' => 'footer_links', /* where in the theme it's assigned */
      'container_class' => 'footer-links clearfix', /* container class */
      'fallback_cb' => 'wp_bootstrap_footer_links_fallback', /* menu fallback */
    'walker' => new Foundation_mega_walker()
    )
  );
}

function secondary_nav() {
  // Display the WordPress menu if available
  wp_nav_menu( 
    array( 
      'menu' => 'secondary_nav', /* menu name */
      'menu_class' => 'secondary-nav',
      'theme_location' => 'secondary_nav', /* where in the theme it's assigned */
      'container' => 'false', /* container class */
      'fallback_cb' => 'wp_bootstrap_expanded_links_fallback', /* menu fallback */
      'walker' => new secondary_walker()
    )
  );
}



// this is the fallback for header menu
function wp_bootstrap_main_nav_fallback() { 
  /* you can put a default here if you like */ 
}

// this is the fallback for footer menu
function wp_bootstrap_footer_links_fallback() { 
  /* you can put a default here if you like */ 
}
// this is the fallback for expanded menu
function wp_bootstrap_expanded_links_fallback() { 
  /* you can put a default here if you like */ 
}
// Shortcodes
require_once('library/shortcodes.php');

// Admin Functions (commented out by default)
// require_once('library/admin.php');         // custom admin functions

// Custom Backend Footer
add_filter('admin_footer_text', 'wp_bootstrap_custom_admin_footer');
function wp_bootstrap_custom_admin_footer() {
  echo '<span id="footer-thankyou">Developed by <a href="http://www.foxfuelcreative.com/" target="_blank">FoxFuel Creative</a></span>.';
}

// adding it to the admin area
add_filter('admin_footer_text', 'wp_bootstrap_custom_admin_footer');

// Set content width
if ( ! isset( $content_width ) ) $content_width = 1400;

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'wpbs-featured', 780, 300, true );
add_image_size( 'wpbs-featured-home', 970, 311, true);
add_image_size( 'wpbs-featured-carousel', 970, 400, true);


/* 
to add more sizes, simply copy a line from above 
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image, 
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function wp_bootstrap_register_sidebars() {
  register_sidebar(array(
    'id' => 'sidebar1',
    'name' => 'Main Sidebar',
    'description' => 'Used on every page BUT the homepage page template.',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));
    
  register_sidebar(array(
    'id' => 'sidebar2',
    'name' => 'Homepage Sidebar',
    'description' => 'Used only on the homepage page template.',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));
    
  register_sidebar(array(
    'id' => 'footer1',
    'name' => 'Footer 1',
    'before_widget' => '<div id="%1$s" class="widget col-sm-4 %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'footer2',
    'name' => 'Footer 2',
    'before_widget' => '<div id="%1$s" class="widget col-sm-4 %2$s">',
    'after_widget' => '</div>',

    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'footer3',
    'name' => 'Footer 3',
    'before_widget' => '<div id="%1$s" class="widget col-sm-4 %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));
    
    
  /* 
  to add more sidebars or widgetized areas, just copy
  and edit the above sidebar code. In order to call 
  your new sidebar just use the following code:
  
  Just change the name to whatever your new
  sidebar's id is, for example:
  
  To call the sidebar in your template, you can just copy
  the sidebar.php file and rename it to your sidebar's name.
  So using the above example, it would be:
  sidebar-sidebar2.php
  
  */
} // don't remove this bracket!
add_action( 'widgets_init', 'wp_bootstrap_register_sidebars' );

/************* COMMENT LAYOUT *********************/
    
// Comment Layout
function wp_bootstrap_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
  <li <?php comment_class(); ?>>
    <article id="comment-<?php comment_ID(); ?>" class="clearfix">
      <div class="comment-author vcard clearfix">
        <div class="avatar col-sm-3">
          <?php echo get_avatar( $comment, $size='75' ); ?>
        </div>
        <div class="col-sm-9 comment-text">
          <?php printf('<h4>%s</h4>', get_comment_author_link()) ?>
          <?php edit_comment_link(__('Edit','wpbootstrap'),'<span class="edit-comment btn btn-sm btn-info"><i class="glyphicon-white glyphicon-pencil"></i>','</span>') ?>
                    
                    <?php if ($comment->comment_approved == '0') : ?>
                <div class="alert-message success">
                  <p><?php _e('Your comment is awaiting moderation.','wpbootstrap') ?></p>
                  </div>
          <?php endif; ?>
                    
                    <?php comment_text() ?>
                    
                    <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time('F jS, Y'); ?> </a></time>
                    
          <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
      </div>
    </article>
    <!-- </li> is added by wordpress automatically -->
<?php
} // don't remove this bracket!

// Display trackbacks/pings callback function
function list_pings($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment;
?>
        <li id="comment-<?php comment_ID(); ?>"><i class="icon icon-share-alt"></i>&nbsp;<?php comment_author_link(); ?>
<?php 

}

/************* SEARCH FORM LAYOUT *****************/

/****************** password protected post form *****/

add_filter( 'the_password_form', 'wp_bootstrap_custom_password_form' );

function wp_bootstrap_custom_password_form() {
  global $post;
  $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
  $o = '<div class="clearfix"><form class="protected-post-form" action="' . get_option('siteurl') . '/wp-login.php?action=postpass" method="post">
  ' . '<p>' . __( "This post is password protected. To view it please enter your password below:" ,'wpbootstrap') . '</p>' . '
  <label for="' . $label . '">' . __( "Password:" ,'wpbootstrap') . ' </label><div class="input-append"><input name="post_password" id="' . $label . '" type="password" size="20" /><input type="submit" name="Submit" class="btn btn-primary" value="' . esc_attr__( "Submit",'wpbootstrap' ) . '" /></div>
  </form></div>
  ';
  return $o;
}

/*********** update standard wp tag cloud widget so it looks better ************/

add_filter( 'widget_tag_cloud_args', 'wp_bootstrap_my_widget_tag_cloud_args' );

function wp_bootstrap_my_widget_tag_cloud_args( $args ) {
  $args['number'] = 20; // show less tags
  $args['largest'] = 9.75; // make largest and smallest the same - i don't like the varying font-size look
  $args['smallest'] = 9.75;
  $args['unit'] = 'px';
  return $args;
}

// filter tag clould output so that it can be styled by CSS
function wp_bootstrap_add_tag_class( $taglinks ) {
    $tags = explode('</a>', $taglinks);
    $regex = "#(.*tag-link[-])(.*)(' title.*)#e";

    foreach( $tags as $tag ) {
      $tagn[] = preg_replace($regex, "('$1$2 label tag-'.get_tag($2)->slug.'$3')", $tag );
    }

    $taglinks = implode('</a>', $tagn);

    return $taglinks;
}

add_action( 'wp_tag_cloud', 'wp_bootstrap_add_tag_class' );

add_filter( 'wp_tag_cloud','wp_bootstrap_wp_tag_cloud_filter', 10, 2) ;

function wp_bootstrap_wp_tag_cloud_filter( $return, $args )
{
  return '<div id="tag-cloud">' . $return . '</div>';
}

// Enable shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

// Disable jump in 'read more' link
function wp_bootstrap_remove_more_jump_link( $link ) {
  $offset = strpos($link, '#more-');
  if ( $offset ) {
    $end = strpos( $link, '"',$offset );
  }
  if ( $end ) {
    $link = substr_replace( $link, '', $offset, $end-$offset );
  }
  return $link;
}
add_filter( 'the_content_more_link', 'wp_bootstrap_remove_more_jump_link' );

// Remove height/width attributes on images so they can be responsive
add_filter( 'post_thumbnail_html', 'wp_bootstrap_remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'wp_bootstrap_remove_thumbnail_dimensions', 10 );

function wp_bootstrap_remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

// Add the Meta Box to the homepage template
function wp_bootstrap_add_homepage_meta_box() {  
  global $post;

  // Only add homepage meta box if template being used is the homepage template
  // $post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : "");
  $post_id = $post->ID;
  $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

  if ( $template_file == 'page-homepage.php' ){
      add_meta_box(  
          'homepage_meta_box', // $id  
          'Optional Homepage Tagline', // $title  
          'wp_bootstrap_show_homepage_meta_box', // $callback  
          'page', // $page  
          'normal', // $context  
          'high'); // $priority  
    }
}

add_action( 'add_meta_boxes', 'wp_bootstrap_add_homepage_meta_box' );

// Field Array  
$prefix = 'custom_';  
$custom_meta_fields = array(  
    array(  
        'label'=> 'Homepage tagline area',  
        'desc'  => 'Displayed underneath page title. Only used on homepage template. HTML can be used.',  
        'id'    => $prefix.'tagline',  
        'type'  => 'textarea' 
    )  
);  

// The Homepage Meta Box Callback  
function wp_bootstrap_show_homepage_meta_box() {  
  global $custom_meta_fields, $post;

  // Use nonce for verification
  wp_nonce_field( basename( __FILE__ ), 'wpbs_nonce' );
    
  // Begin the field table and loop
  echo '<table class="form-table">';

  foreach ( $custom_meta_fields as $field ) {
      // get value of this field if it exists for this post  
      $meta = get_post_meta($post->ID, $field['id'], true);  
      // begin a table row with  
      echo '<tr> 
              <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
              <td>';  
              switch($field['type']) {  
                  // text  
                  case 'text':  
                      echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="60" /> 
                          <br /><span class="description">'.$field['desc'].'</span>';  
                  break;
                  
                  // textarea  
                  case 'textarea':  
                      echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="80" rows="4">'.$meta.'</textarea> 
                          <br /><span class="description">'.$field['desc'].'</span>';  
                  break;  
              } //end switch  
      echo '</td></tr>';  
  } // end foreach  
  echo '</table>'; // end table  
}  

// Save the Data  
function wp_bootstrap_save_homepage_meta( $post_id ) {  

    global $custom_meta_fields;  
  
    // verify nonce  
    if ( !isset( $_POST['wpbs_nonce'] ) || !wp_verify_nonce($_POST['wpbs_nonce'], basename(__FILE__)) )  
        return $post_id;

    // check autosave
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;

    // check permissions
    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) )
            return $post_id;
        } elseif ( !current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
    }
  
    // loop through fields and save the data  
    foreach ( $custom_meta_fields as $field ) {
        $old = get_post_meta( $post_id, $field['id'], true );
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta( $post_id, $field['id'], $new );
        } elseif ( '' == $new && $old ) {
            delete_post_meta( $post_id, $field['id'], $old );
        }
    } // end foreach
}
add_action( 'save_post', 'wp_bootstrap_save_homepage_meta' );

// Add thumbnail class to thumbnail links
function wp_bootstrap_add_class_attachment_link( $html ) {
    $postid = get_the_ID();
    $html = str_replace( '<a','<a class="thumbnail"',$html );
    return $html;
}
add_filter( 'wp_get_attachment_link', 'wp_bootstrap_add_class_attachment_link', 10, 1 );

// Add lead class to first paragraph
function wp_bootstrap_first_paragraph( $content ){
    global $post;

    // if we're on the homepage, don't add the lead class to the first paragraph of text
    if( is_page_template( 'page-homepage.php' ) )
        return $content;
    else
        return preg_replace('/<p([^>]+)?>/', '<p$1 class="">', $content, 1);
}
add_filter( 'the_content', 'wp_bootstrap_first_paragraph' );

// Menu output mods
class Bootstrap_walker extends Walker_Nav_Menu {

  function start_el(&$output, $object, $depth = 0, $args = Array(), $current_object_id = 0){

   global $wp_query;
   $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
  
   $class_names = $value = '';
  
    // If the item has children, add the dropdown class for bootstrap
    if ( $args->has_children ) {
      $class_names = "dropdown ";
    }
  
    $classes = empty( $object->classes ) ? array() : (array) $object->classes;
    
    $class_names .= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $object ) );
    $class_names = ' class="'. esc_attr( $class_names ) . '"';
       
    $output .= $indent . '<li id="menu-item-'. $object->ID . '"' . $value . $class_names .'>';

    $attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
    $attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
    $attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
    $attributes .= ! empty( $object->url )        ? ' href="'   . esc_attr( $object->url        ) .'"' : '';

    // if the item has children add these two attributes to the anchor tag
    if ( $args->has_children ) {
      $attributes .= ' class="dropdown-toggle" data-toggle="dropdown"';
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before .apply_filters( 'the_title', $object->title, $object->ID );
    $item_output .= $args->link_after;

    // if the item has children add the caret just before closing the anchor tag
    if ( $args->has_children ) {
      $item_output .= '<b class="caret"></b></a>';
    }
    else {
      $item_output .= '</a>';
    }

    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $object, $depth, $args );
  } // end start_el function
        
  function start_lvl(&$output, $depth = 0, $args = Array()) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"dropdown-menu\">\n";
  }
      
  function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
    $id_field = $this->db_fields['id'];
    if ( is_object( $args[0] ) ) {
        $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
    }
    return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
  }        
}




// Menu output mods
class Foundation_mega_walker extends Walker_Nav_Menu{

  function start_el(&$output, $object, $depth = 0, $args = Array(), $current_object_id = 0){

   global $wp_query;
   $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
  
   $class_names = $value = '';
  
    // If the item has children, add the dropdown class for bootstrap
    if ( $args->has_children ) {
      $class_names = "has-kids ";
    }
  
    $classes = empty( $object->classes ) ? array() : (array) $object->classes;
      if( in_array('start-column', $classes) ){
      $output .= '<div class="medium-3 columns">';
    }
      if( in_array('new-column', $classes) ){
      $output .= '</div><div class="medium-3 columns">';
    }
      if( in_array('end-column', $classes) ){
      $output .= '</div><div class="medium-3 columns end">';
    }
    
    $class_names .= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $object ) );
    $class_names = ' class="'. esc_attr( $class_names ) . '"';
       
    $output .= $indent . '<li id="menu-item-'. $object->ID . '"' . $value . $class_names .'>';

    $attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
    $attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
    $attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
    $attributes .= ! empty( $object->url )        ? ' href="'   . esc_attr( $object->url        ) .'"' : '';

    // if the item has children add these two attributes to the anchor tag
    if ( $args->has_children ) {
      $attributes .= ' class="test" ';
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before .apply_filters( 'the_title', $object->title, $object->ID );
    $item_output .= $args->link_after;

    // if the item has children add the caret just before closing the anchor tag
    if ( $args->has_children ) {
      $item_output .= '<b class="caret"></b></a>';
    }
    else {
      $item_output .= '</a>';
    }

    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $object, $depth, $args );
  } // end start_el function
        
  function start_lvl(&$output, $depth = 0, $args = Array()) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"fmm-subnav\">\n";
  }
      
  function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
    $id_field = $this->db_fields['id'];
    if ( is_object( $args[0] ) ) {
        $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
    }
    return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
  }        
}




class secondary_walker extends Walker_Nav_Menu
{
      function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
      {
           global $wp_query;
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

           $class_names = $value = '';

           $classes = empty( $item->classes ) ? array() : (array) $item->classes;

           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
           $class_names = ' class="'. esc_attr( $class_names ) . '"';

           $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

           $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
           $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
           $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
           $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

           $prepend = '<strong>';
           $append = '</strong>';
           $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

           if($depth != 0)
           {
             $description = $append = $prepend = "";
           }

            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
            $item_output .= $description.$args->link_after;
            $item_output .= ' '.$item->subtitle.'</a>';
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
            }

  public function end_el( &$output, $object, $depth = 0, $args = array() ) {
    
  }
  function start_lvl(&$output, $depth = 0, $args = Array()) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"dropdown-menu\">\n";
  }
      
  public function end_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "$indent</ul>\n";
  }

}


add_editor_style('editor-style.css');

function wp_bootstrap_add_active_class($classes, $item) {
  if( $item->menu_item_parent == 0 && (in_array('current-menu-item', $classes)) ) {
    $classes[] = "active";
  }
  
  return $classes;
}

// Add Twitter Bootstrap's standard 'active' class name to the active nav link item
add_filter('nav_menu_css_class', 'wp_bootstrap_add_active_class', 10, 2 );

// enqueue styles
if( !function_exists("wp_bootstrap_theme_styles") ) {  
    function wp_bootstrap_theme_styles() { 

      wp_register_style( 'wpbs', get_template_directory_uri() . '/css/app.css', array(), '2.2', 'all' );
      wp_enqueue_style( 'wpbs' );
      /*wp_register_style( 'additional', get_template_directory_uri() . '/css/additional.css', array(), '1.4', 'all' );
      wp_enqueue_style( 'additional' );  */

      // For child themes
      wp_register_style( 'wpbs-style', get_stylesheet_directory_uri() . '/style.css', array(), '1.1', 'all' );
      wp_enqueue_style( 'wpbs-style' );
    }
}
add_action( 'wp_enqueue_scripts', 'wp_bootstrap_theme_styles' );


// enqueue javascript
if( !function_exists( "wp_bootstrap_theme_js" ) ) {  
  function wp_bootstrap_theme_js(){

    if ( !is_admin() ){
      if ( is_singular() AND comments_open() AND ( get_option( 'thread_comments' ) == 1) ) 
        wp_enqueue_script( 'comment-reply' );
    }

    // This is the full Bootstrap js distribution file. If you only use a few components that require the js files consider loading them individually instead
    wp_register_script( 'jq2', 
      get_template_directory_uri() . '/bower_components/jquery/dist/jquery.min.js', 
      array('jquery'), 
      '1.1' );
    
    /*
    wp_register_script( 'what-input', 
      get_template_directory_uri() . '/bower_components/what-input/what-input.js', 
      array('jquery'), 
      '1.1' );
    */

    wp_register_script( 'wpbs-js', 
      get_template_directory_uri() . '/bower_components/foundation-sites/dist/foundation.min.js',
      array('jquery'), 
      '1.4' );
  
    
  
    wp_enqueue_script( 'jq2' );
    //wp_enqueue_script( 'what-input' );
    wp_enqueue_script( 'wpbs-js' );
    
  }
}
add_action( 'wp_enqueue_scripts', 'wp_bootstrap_theme_js' );

// Get <head> <title> to behave like other themes
function wp_bootstrap_wp_title( $title, $sep ) {
  global $paged, $page;

  if ( is_feed() ) {
    return $title;
  }

  // Add the site name.
  $title .= get_bloginfo( 'name' );

  // Add the site description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) ) {
    $title = "$title $sep $site_description";
  }

  // Add a page number if necessary.
  if ( $paged >= 2 || $page >= 2 ) {
    $title = "$title $sep " . sprintf( __( 'Page %s', 'wpbootstrap' ), max( $paged, $page ) );
  }

  return $title;
}
add_filter( 'wp_title', 'wp_bootstrap_wp_title', 10, 2 );

// Related Posts Function (call using wp_bootstrap_related_posts(); )
function wp_bootstrap_related_posts() {
  echo '<ul id="bones-related-posts">';
  global $post;
  $tags = wp_get_post_tags($post->ID);
  if($tags) {
    foreach($tags as $tag) { $tag_arr .= $tag->slug . ','; }
        $args = array(
          'tag' => $tag_arr,
          'numberposts' => 5, /* you can change this to show more */
          'post__not_in' => array($post->ID)
      );
        $related_posts = get_posts($args);
        if($related_posts) {
          foreach ($related_posts as $post) : setup_postdata($post); ?>
              <li class="related_post"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
          <?php endforeach; } 
      else { ?>
            <li class="no_related_post">No Related Posts Yet!</li>
    <?php }
  }
  wp_reset_query();
  echo '</ul>';
}

// Numeric Page Navi (built into the theme by default)
function wp_bootstrap_page_navi($before = '', $after = '') {
  global $wpdb, $wp_query;
  $request = $wp_query->request;
  $posts_per_page = intval(get_query_var('posts_per_page'));
  $paged = intval(get_query_var('paged'));
  $numposts = $wp_query->found_posts;
  $max_page = $wp_query->max_num_pages;
  if ( $numposts <= $posts_per_page ) { return; }
  if(empty($paged) || $paged == 0) {
    $paged = 1;
  }
  $pages_to_show = 7;
  $pages_to_show_minus_1 = $pages_to_show-1;
  $half_page_start = floor($pages_to_show_minus_1/2);
  $half_page_end = ceil($pages_to_show_minus_1/2);
  $start_page = $paged - $half_page_start;
  if($start_page <= 0) {
    $start_page = 1;
  }
  $end_page = $paged + $half_page_end;
  if(($end_page - $start_page) != $pages_to_show_minus_1) {
    $end_page = $start_page + $pages_to_show_minus_1;
  }
  if($end_page > $max_page) {
    $start_page = $max_page - $pages_to_show_minus_1;
    $end_page = $max_page;
  }
  if($start_page <= 0) {
    $start_page = 1;
  }
    
  echo $before.'<div class="text-center"><ul class="pagination" role="navigation" aria-label="Pagination">'."";
  if ($paged > 1) {
    $first_page_text = '<i class="fa fa-caret-left" aria-hidden="true"></i><i class="fa fa-caret-left" aria-hidden="true"></i>';
    echo '<li class="prev"><a href="'.get_pagenum_link().'" title="' . __('First','wpbootstrap') . '">'.$first_page_text.'</a></li>';
  }
    
  $prevposts = get_previous_posts_link( __('<i class="fa fa-caret-left" aria-hidden="true"></i>&nbsp;&nbsp;Previous','wpbootstrap') );
  if($prevposts) { 
    echo '<li>' . $prevposts  . '</li>'; 
  } else { 
    echo '<li class="disabled">' . __('<i class="fa fa-caret-left" aria-hidden="true"></i>&nbsp;&nbsp;Previous','wpbootstrap') . '</li>'; 
  }
  
  for($i = $start_page; $i  <= $end_page; $i++) {
    if($i == $paged) {
      echo '<li class="active"><span class="active-page">'.$i.'</span></li>';
    } else {
      echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
    }
  }
  echo '<li class="">';
  next_posts_link( __('Next&nbsp;&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i>','wpbootstrap') );
  echo '</li>';
  if ($end_page < $max_page) {
    $last_page_text = "&raquo;";
    echo '<li class="next"><a href="'.get_pagenum_link($max_page).'" title="' . __('Last','wpbootstrap') . '">'.$last_page_text.'</a></li>';
  }
  echo '</ul></div>'.$after."";
}

// Remove <p> tags from around images
function wp_bootstrap_filter_ptags_on_images( $content ){
  return preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
}
add_filter( 'the_content', 'wp_bootstrap_filter_ptags_on_images' );



/*

 ______   ______     __         ______     ______     ______   ______  
/\__  _\ /\  __ \   /\ \       /\  == \   /\  __ \   /\__  _\ /\__  _\ 
\/_/\ \/ \ \  __ \  \ \ \____  \ \  __<   \ \ \/\ \  \/_/\ \/ \/_/\ \/ 
   \ \_\  \ \_\ \_\  \ \_____\  \ \_____\  \ \_____\    \ \_\    \ \_\ 
    \/_/   \/_/\/_/   \/_____/   \/_____/   \/_____/     \/_/     \/_/ 






*/

/*------------------------------------------------------TALBOTT FUNCTIONS---------------------------------------------------------------------------------------*/


/*--------MISC FUNCS ------------------------*/

function get_page_id_outside_loop(){
  global $post;
  return $post->ID;
}

wpcf7_add_shortcode('assessment_data', 'wpcf7_sourceurl_shortcode_handler', true);

function wpcf7_sourceurl_shortcode_handler($tag) {
  //if (!is_array($tag)) return '';

  $name = $tag['name'];
  if (empty($name)) return '';

  $html = '<input id="assessment_data" type="hidden" name="' . $name . '" value="" />';
  return $html;
}


function is_chat_live() {
  date_default_timezone_set('America/Chicago');
  $currentDate = new DateTime();
  if($currentDate->format('N') >= 6) {
    $start_str = get_field('weekend_chat_start_time', 'option');
    $end_str = get_field('weekend_end_time', 'option');
  } else {
    $start_str = get_field('chat_start_time', 'option');
    $end_str = get_field('chat_end_time', 'option');
  }
  
  $chat_start = DateTime::createFromFormat('H:i', '00:00');
  $chat_end = DateTime::createFromFormat('H:i', '00:00');
  
  $start_parts = explode(':', $start_str);
  $end_parts = explode(':', $end_str);
  
  $start_mod = ($start_parts[0] > 0) ? '+'.$start_parts[0].' hours ' : '' ;
  $start_mod .= ($start_parts[1] > 0) ? '+'.$start_parts[1].' minutes ' : '' ;
  
  $end_mod = ($end_parts[0] > 0) ? '+'.$end_parts[0].' hours ' : '' ;
  $end_mod .= ($end_parts[1] > 0) ? '+'.$end_parts[1].' minutes ' : '' ;
  
  $chat_start->modify($start_mod);
  $chat_end->modify($end_mod);
  
  if ($currentDate > $chat_start && $currentDate < $chat_end) {
    return true;
  } else {
    return false;
  }
}


function display_phone_number($number) {
  $stripped_number = preg_replace("/[^0-9]/","",$number);
  if(strlen($stripped_number) >= 10 && strlen($stripped_number) <= 11) {
    if(preg_match("/^1/", $stripped_number)) {
      $stripped_number = substr($stripped_number, 1);
    }
    $formatted_number = ''.substr($stripped_number, -10, 3).'-'.substr($stripped_number, -7, 3).'-'.substr($stripped_number, -4, 4);
  } else {
    $formatted_number = $number;
  }
  return $formatted_number;
}

//used to finds the most veiwed post in each cat
function prepare_sidebar_posts(){
  $categories = get_categories( array(
    'orderby' => 'name',
    'order'   => 'ASC'
  ) );
  $category_arr = array();
  foreach ( $categories as $category ) {
      $cat = array(
        'category'=>$category,
        'posts'=>get_posts( array('category'=>$category->term_id, 'posts_per_page'=>5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC') )
      );
    if( !empty($cat['posts'])){
      $category_arr[] = $cat;
    }
    
  }
  return $category_arr;
}


add_action( 'init', 'my_new_default_post_type', 1 );
function my_new_default_post_type() {
 
    register_post_type( 'post', array(
        'labels' => array(
            'name_admin_bar' => _x( 'Post', 'add new on admin bar' ),
        ),
        'public'  => true,
        '_builtin' => true, 
        '_edit_link' => 'post.php?post=%d', 
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'rewrite' => array( 'slug' => '' ),
        'query_var' => false,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats' ),
    ) );
}


//used to give each post a "most viewed" count
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);



//get the youtube code from video for embeds
function get_youtube_code($url){
  preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
  return $matches[1];

}



//ADDITIONAL IMAGE SIZES
if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'extra-large', 800, 400, true ); //(cropped)
  add_image_size( 'xxl', 1200, 800, true ); //(cropped)
}

add_filter('image_size_names_choose', 'my_image_sizes');

function my_image_sizes($sizes) {
  $addsizes = array(
  "extra-large" => __( "Extra Large"),
  "xxl" => __( "XXL")
  );
  $newsizes = array_merge($sizes, $addsizes);
  return $newsizes;
}


function add_custom_files() {
   // wp_enqueue_style( 'style-name', get_stylesheet_uri() );
   wp_enqueue_script( 'talbott.js', get_template_directory_uri() . '/library/js/talbott.js', array(), '1.6.0', true );
  wp_enqueue_script( 'moment.js', get_template_directory_uri() . '/library/js/moment.js', array(), '1.3.0', true );
  wp_enqueue_script( 'chat.js', get_template_directory_uri() . '/library/js/chat.js', array(), '1.3.0', true );
}
add_action( 'wp_enqueue_scripts', 'add_custom_files' );


// Register Google Maps API key
function my_acf_google_map_api( $api ){
  $api['key'] = 'AIzaSyCuGEkS3XOItflpYiLMK5ClgulNVI1l9TM';
  return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


//function for excluding from search
function my_search_filter($wp_query) {
   if ($wp_query->is_search) {
      $wp_query->set('post__not_in', array( -6551 ) );
   }
   return $wp_query;
}
add_filter('pre_get_posts','my_search_filter');


/*-------- END MISC FUNCS ------------------------*/



/* --------------------------------------------CUSTOM MOBILE MENU WALKER ------------------------------------------------------------------------*/


//used to display the main nav on mobile
function mobile_nav() {
  // Display the WordPress menu if available
  wp_nav_menu( 
    array( 
      'menu' => 'main_nav', /* menu name */
      'menu_class' => 'mobile-nav menu dropdown ',
      'theme_location' => 'main_nav', /* where in the theme it's assigned */
      'container' => 'false', /* container class */
      'fallback_cb' => 'wp_bootstrap_expanded_links_fallback', /* menu fallback */
      'walker' => new mobile_menu_walker()
    )
  );
}


// Used for the main site menu on mobile
class mobile_menu_walker extends Walker_Nav_Menu {

  function start_el(&$output, $object, $depth = 0, $args = Array(), $current_object_id = 0){

   global $wp_query;
   $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
  
   $class_names = $value = '';
  
    // If the item has children, add the dropdown class for bootstrap
    if ( $args->has_children ) {
      $class_names = "dropdown ";
    }
  
    $classes = empty( $object->classes ) ? array() : (array) $object->classes;
    
    $class_names .= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $object ) );
    $class_names = ' class="'. esc_attr( $class_names ) . '"';
       
    $output .= $indent . '<li id="menu-item-'. $object->ID . '"' . $value . $class_names .'>';

    $attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
    $attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
    $attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
    $attributes .= ! empty( $object->url )        ? ' href="'   . esc_attr( $object->url        ) .'"' : '';

    // if the item has children add these two attributes to the anchor tag
    if ( $args->has_children ) {
      $attributes .= ' ';
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before .apply_filters( 'the_title', $object->title, $object->ID );
    $item_output .= $args->link_after;

    // if the item has children add the caret just before closing the anchor tag
    if ( $args->has_children ) {
      $item_output .= '<i class="fa fa-sort-desc" aria-hidden="true"></i></a>';
    }
    else {
      $item_output .= '</a>';
    }

    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $object, $depth, $args );
  } // end start_el function
        
  function start_lvl(&$output, $depth = 0, $args = Array()) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"menu\" >\n";
  }
      
  function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
    $id_field = $this->db_fields['id'];
    if ( is_object( $args[0] ) ) {
        $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
    }
    return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
  }        
}

/* --------------------------------------------END CUSTOM MENU WALKER---------------------------------------*/




/*-------------------- FUNCTIONS TO REVTRIEVE PAGE / POST IMAGES  -------------*/
function get_page_banner(){
  global $post;
  //does this page have a banner?
  $banner = get_field('top_banner', $post->ID);
  if($banner){
    return $banner['url'];
  } else{
    //does the parent page have a banner?
    $parent_id = $post->post_parent;
    $banner = get_field('top_banner', $parent_id);
    if ($banner){
      return $banner['url'];
    }else{
      //get this from globals;
      $banner = get_field('default_banner', 'option');
      $url = $banner['url'];
      return $url;
    }
  }
  
}

function get_post_banner(){
  global $post;
  
  //does this page have a banner?
  $banner = get_field('top_banner', $post->ID);
  if($banner){
    return $banner['url'];
  } else{
      $banner = get_field('default_banner', 'option');
      $url = $banner['url'];
      return $url;
  }
  
}

//this function returns the image for both pages and post, and those are the only two content types with banner images
function get_image_banner(){
  global $post;
  //does this page have a banner?
  $banner = get_field('banner_image', $post->ID);
  if($banner){
    return $banner['url'];
  } else{
      $banner = get_field('default_banner', 'option');
      $url = $banner['url'];
      return $url;
  }
  
}
/*-------------------- END FUNCTIONS TO REVTRIEVE PAGE / POST IMAGES  -------------*/




/* --------------------------------------latest news ----------------------------------*/
function the_excerpt_max_charlength($charlength, $excerpt) {

  $charlength++;
  $excer = '';
  if ( mb_strlen( $excerpt ) > $charlength ) {
    $subex = mb_substr( $excerpt, 0, $charlength - 5 );
    $exwords = explode( ' ', $subex );
    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    if ( $excut < 0 ) {
      $excer =  mb_substr( $subex, 0, $excut );
    } else {
      $excer =  $subex;
    }
    return $excer.'[...]';
  } else {
    return $excerpt;
  }
}
function get_latest_news($exclude_id = false){
  $args = array(
    
    'posts_per_page' => 1, 
    'post_type'=>'news'
  );
  if( !empty($exclude_id) ){
    $args['exclude'] = $exclude_id;
  }
  $the_query = new WP_Query( $args );
  $resources = array();
  if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {
      $the_query->the_post();
      $feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
      //var_dump( $feat_image );
      if($feat_image){
        $url = $feat_image;
      } else{
        $feat_image = get_field('default_resource_image', 'option');
        $url = $feat_image['url'];
      }
      $excerpt = the_excerpt_max_charlength(104, get_the_excerpt());
      $resource = array(
        'title'=>get_the_title(),
        'featured_image'=>$url,
        'id'=>get_the_id(),
        'url'=>get_the_permalink(),
        'excerpt'=>$excerpt
      );
      $resources[] = $resource;
    }//while
  }//if
  wp_reset_postdata();
  return $resources;
}


/* --------------------------------------get work with us ----------------------------------*/
function get_work_snippet(){
  $args = array(
    
    'posts_per_page' => 1, 
    'page_id'=>6551
  );

  $the_query = new WP_Query( $args );
  $resources = array();
  if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {
      $the_query->the_post();
      $feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
      //var_dump( $feat_image );
      if($feat_image){
        $url = $feat_image;
      } else{
        
        $url = false;
      }
      $page = array(
        'title'=>get_the_title(),
        'featured_image'=>$url,
        'id'=>get_the_id(),
        'url'=>get_the_permalink(),
        'excerpt'=>get_the_excerpt()
      );
      $resources[] = $page;
    }//while
  }//if
  wp_reset_postdata();
  //return a single page
  return $resources[0];
}

/*-------------------- Staff member FUNCTIONS  -------------*/
function get_staff_members_from_location(){
  global $post;
  //does this page have a banner?
  $staff_members = get_field('connected_staff_members', $post->ID);

  $staff_members_arr = [];
  foreach($staff_members as $member){
    $sm = array(
      'title'=>get_the_title($member->ID),
      'job_title'=>get_field('job_title', $member->ID),
      'image'=>wp_get_attachment_url( get_post_thumbnail_id($member->ID ) ),
      'permalink'=>get_the_permalink($member->ID )
    );
    $staff_members_arr[] = $sm;
  }
  return $staff_members_arr;
  
}

/*-------------------- END Staff member FUNCTIONS  -------------*/

/*-------------------- SHORTCODE FUNCTIONS  -------------*/
include_once( 'includes/callout_functions.php' );


// [bartag foo="foo-value"]
function place_button_func( $atts ) {
  $url = $atts['url'];
  if (empty($url)){
    return false;
  }
  $text = $atts['text'];
  $theme = $atts['theme'];
  if (empty($theme)){
    $theme = 'dark';
  }
  if($theme == 'light'){
    $class ='secondary';
  } else{
    $class ='';
  }
  if (empty($text)){
    $text = 'Learn More';
  }
  return '<a href="'.$url.'" class="button hollow '.$class.'">'.$text.'</a>';
}
add_shortcode( 'place_button', 'place_button_func' );


function place_assessment_func( $atts ) {
ob_start();
?>

              <div id="assess-contact" class="">

                <div class="row ">
                  <div class="medium-6 columns">
                    <div id="assessment-shell">

                    </div>
                  </div>
                  <div class="medium-6 columns" id="home-contact-form">
                    <?php 
                      
                      echo do_shortcode( '[contact-form-7 id="6500" title="Treatment Contact / Assessment Form"]' );
                    ?>
                  </div>
                </div>
              </div>  
<?php 
 $html = ob_get_contents();
ob_end_clean();
  return $html;
}
add_shortcode( 'place_assessment', 'place_assessment_func' );
/*-------------------- END SHORTCODE FUNCTIONS  -------------*/


//updates to wordpress tiny MCE
function my_mce_buttons_2( $buttons ) { 
  /**
   * Add in a core button that's disabled by default
   */
  $buttons[] = 'superscript';
  $buttons[] = 'subscript';
  $buttons[] = 'fontsizeselect';
  $buttons[] = 'fontselect';

  return $buttons;
}
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );


//mobile ditection
require_once 'includes/Mobile_Detect.php';
$detect = new Mobile_Detect;
function is_mobile(){
  global $detect;
  return  $detect->isMobile();
}



/*-------------------- ACF SEMINAR ADMIN FEATURES  -------------*/

add_filter('acf/update_value/type=date_time_picker', 'frn_update_value_date_time_picker', 10, 3);
function frn_update_value_date_time_picker( $value, $post_id, $field ) {
  //date_default_timezone_set ( "Etc/UTC");
  return strtotime($value);
}

//Sessions
add_filter('manage_edit-seminar_columns', 'frn_add_acf_to_tables_head');
function frn_add_acf_to_tables_head( $columns ) {
    //creates the column in the table (no data)
    $columns['start_time']  = 'Date of Seminar';
    return $columns;
}
add_action( 'manage_seminar_posts_custom_column', 'frn_add_acf_to_sessions_values', 10, 2);
function frn_add_acf_to_sessions_values( $column_name, $post_id ) {
      //Displays the date/time data saved in the DB for each seminar
      //This function runs each time a column is printed, not just once for every row, but for every table cell.
      if( $column_name == 'start_time' ) {
        
        $start_time = get_post_meta( $post_id, 'start_time', true );
        if($start_time!=="") {
          if((string)(int)$start_time === (string)$start_time) {
            //timezone is set to default for typical timecodes
            //since user enters time according to the actual time the seminar will happen, we don't need to consider converting the time back to a timezone
            //The only time we might need to convert is when we are comparing times to the current time and returning the difference. Daylight savings time would have an effect in that scenario (but we aren't comparing, so we don't have to worry about it.)
            date_default_timezone_set ( "Etc/UTC" );
            $date=trim(date ('F j, Y g:ia', $start_time));
          }
          else {
            $date=trim(date ('F j, Y g:ia', strtotime($start_time)));
          }
          if($date!=="") echo $date; //." (DB: ".$start_time. "; ".get_query_var( 'post_type' ).")";
        }
      }
}
add_filter( 'manage_edit-seminar_sortable_columns', 'frn_add_acf_to_sessions_sort' );
function frn_add_acf_to_sessions_sort( $columns ) {
    $columns['start_time'] = array('start_time',1);
    return $columns;
}
if(is_admin()) add_action( 'pre_get_posts', 'frn_orderby_date' );
function frn_orderby_date( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get( 'orderby');
  if(get_query_var( 'post_type' ) == 'seminar') {
    if ( 'start_time' == $orderby || $orderby=="" ) {
      $query->set( 'meta_key', 'start_time' );
      $query->set( 'orderby', 'meta_value_num' );
    }
  }
}
function frn_convert_timezone($time, $timezoneRequired, $format) {
    //taken from: https://stackoverflow.com/questions/3905193/convert-time-and-date-from-one-time-zone-to-another-in-php
    //This function isn't used, but saved here in case we ever need to compare times between the seminar and today's time (date isn't that sensitive to daylight savings unless our situation was around midnight)
    $dayLightFlag = false;
    $dayLgtSecCurrent = $dayLgtSecReq = 0;
    $system_timezone = date_default_timezone_get();
    //local_timezone = $currentTimezone;
    date_default_timezone_set("Etc/UTC");
    $local = date("Y-m-d H:i:s");
    /* Uncomment if daylight is required */
    //        $daylight_flag = date("I", strtotime($time));
    //        if ($daylight_flag == 1) {
    //            $dayLightFlag = true;
    //            $dayLgtSecCurrent = -3600;
    //        }
    date_default_timezone_set("GMT");
    $gmt = date("Y-m-d H:i:s ");

    $require_timezone = $timezoneRequired;
    date_default_timezone_set($require_timezone);
    $required = date("Y-m-d H:i:s ");
    /* Uncomment if daylight is required */
    //        $daylight_flag = date("I", strtotime($time));
    //        if ($daylight_flag == 1) {
    //            $dayLightFlag = true;
    //            $dayLgtSecReq = +3600;
    //        }

    date_default_timezone_set($system_timezone);

    $diff1 = (strtotime($gmt) - strtotime($local));
    $diff2 = (strtotime($required) - strtotime($gmt));

    $date = new DateTime($time);

    $date->modify("+$diff1 seconds");
    $date->modify("+$diff2 seconds");

    if ($dayLightFlag) {
        $final_diff = $dayLgtSecCurrent + $dayLgtSecReq;
        $date->modify("$final_diff seconds");
    }

    $timestamp = $date->format($format);

    return $timestamp;
}

// Adding ACF Options Page

if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page();
  
}


?>