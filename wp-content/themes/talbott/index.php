<?php get_header(); ?>
			
			<div class="clearfix row" data-equalizer="outer" data-equalize-on="large">
			
				<?php get_sidebar(); // sidebar 1 ?>
				
				<div id="main" class="large-9 columns clearfix float-left" role="main" data-equalizer-watch="outer">
					<?php $post_count = 0; ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php 
							$post_count++;
							$bsrc = get_field('thumbnail_image');
							$banner_src = (!empty($bsrc)) ? $bsrc : get_field('default_resource_image', 'option');
						?>
						<div class="row" data-equalizer="inner" data-equalize-on="medium">
							<a href="<?php the_permalink() ?>" class="large-6 columns show-for-large <?php echo ($post_count %2 == 0) ? 'float-right' : '' ; ?>" style="display:block; background: url('<?php echo $banner_src['url']; ?>') no-repeat center center / cover;" data-equalizer-watch="inner">
								
							</a>
							<div class="large-6 columns <?php echo ($post_count %2 == 0) ? 'float-left text-right' : 'rl-right-pad' ; ?>" style="" data-equalizer-watch="inner">
								<div class="tb-pad-40 resource-list-text <?php echo ($post_count %2 == 0) ? 'rlt-right' : '' ; ?>">
									<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
									
										<header>
											<div class="page-header"><h2 class=""><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2></div>
										</header> <!-- end article header -->
									
										<section class="post_content clearfix">
											<img src="<?php echo $banner_src['url']; ?>" alt="" class="hide-for-large resources-list-mobile-img">
											<?php the_excerpt(); ?>
											<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="button hollow">Read More</a>
										</section> <!-- end article section -->
										
										<footer>
											<p class="tags hide"><?php the_tags('<span class="tags-title">' . __("Tags","wpbootstrap") . ':</span> ', ' ', ''); ?></p>
										</footer> <!-- end article footer -->
									
									</article> <!-- end article -->
								</div>
							</div>
						</div>
							
					
					<?php endwhile; ?>	
					
					<?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>
						
						<?php wp_bootstrap_page_navi(); // use the page navi function ?>
						
					<?php } else { // if it is disabled, display regular wp prev & next links ?>
						<nav class="wp-prev-next">
							<ul class="pager">
								<li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
								<li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
							</ul>
						</nav>
					<?php } ?>		
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				
    
			</div> <!-- end #content -->

<?php get_footer(); ?>