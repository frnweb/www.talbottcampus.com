<form action="<?php echo home_url( '/' ); ?>" method="get" class="form-inline search-form">
	<div class="input-group">
		<span class="input-group-label show-for-lt-ie9">Search</span>
		<input class="input-group-field form-control" name="s" id="search" placeholder="<?php _e("Search...","wpbootstrap"); ?>" value="<?php the_search_query(); ?>" type="text">
		<div class="input-group-button">
			<button type="submit" class="button"><?php _e("Go","wpbootstrap"); ?></button>
		</div>
	</div>
</form>