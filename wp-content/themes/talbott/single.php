<?php get_header(); ?>
			
			
			
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php 
						//page views
						wpb_set_post_views(get_the_ID());

						//get banner
						$src = get_image_banner();
					?>
					<?php if(get_field('use_image_banner')): ?>
						<header>
							<div class="page-header interior-top-banner blog-stretchy-wrapper" style="background: url('<?php echo $src; ?>') center center / cover;">
								<div>
									<div class="row interior-top-text-box">
										<div class="small-12 columns">
											<div style="display: table; width: 100%;">
												<div style="display: table-cell; vertical-align: middle;">
													<h1 class="single-title" style="color: #fff;" itemprop="headline"><?php the_title(); ?></h1>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</header> <!-- end article header -->
					<?php endif; ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
						<div class="clearfix row" data-equalizer="outer" data-equalize-on="large">
							<?php get_sidebar(); // sidebar 1 ?>
							
							<div id="main" class="large-9 columns tb-pad-30 float-left" role="main" data-equalizer-watch="outer">
								<?php if(get_field('use_image_banner') == false): ?>
									<header>
										<h1 itemprop="headline" class="tb-pad-30"><?php the_title(); ?></h1>
									</header>
								<?php endif; ?>
								<section class="post_content clearfix" itemprop="articleBody">
									
									<?php the_content(); ?>
									<?php wp_link_pages(); ?>
									
									
									<?php 
									// only show edit button if user has permission to edit posts
									if( $user_level > 0 ) { 
									?>
										<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post"><i class="icon-pencil icon-white"></i> <?php _e("Edit post","wpbootstrap"); ?></a>
									<?php } ?>
								</section> <!-- end article section -->
							</div> <!-- end #main -->
								
						</div>
						
					</article> <!-- end article -->
					<?php echo get_resources_block(); ?>
					
					<?php //comments_template('',true); ?>
					
					<?php endwhile; ?>			
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				 <!-- end #content -->

<?php get_footer(); ?>