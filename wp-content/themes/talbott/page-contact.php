<?php
/*
Template Name: Contact Page Template
*/
?>
<?php get_header(); ?>
<style type="text/css">

.acf-map {
	width: 100%;
	min-height: 200px;
}
@media (max-width: 620px) {
	.acf-map {
		width: 100%;
		min-height: 400px;
	}
}
/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php 
					$src = get_page_banner();
					$curHPPostID = get_the_ID();
				?>
				
					<div id="main" class="" role="main">
						<div class="row expanded collapse" id="post-<?php the_ID(); ?>" data-equalizer data-equalize-on="medium">
							<?php 
								$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
							?>
							<?php if( !empty( $feat_image )):?>
							
								<div class="medium-6 large-8 columns" data-equalizer-watch>
									<div class="row tb-pad-60">
										<div class="large-10 large-centered columns">
											<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
											<?php the_content(); ?>
											<?php wp_link_pages(); ?>
										</div>
									</div>									
								</div>
								<div class="medium-6 large-4 columns" id="contact-page-image" style="background-image: url('<?php echo $feat_image ?>')" data-equalizer-watch>
								</div>
							
							<?php else: ?>
							
								<div class="medium-12 columns">
									<?php the_content(); ?>
									<?php wp_link_pages(); ?>															
								</div>

							
							<?php endif; ?>
						</div>

							
						<?php
						$locations_arr = array();
							//insurance content type
							$args = array(
								'post_type'=>'location',
								'orderby'=>'menu_order',
								'order'=>'asc'
							);
							$locations = new WP_Query( $args );
							if($locations->have_posts()) : 
						?>
						<div class="row expanded collapse" data-equalizer data-equalize-on="medium">
							<div class="medium-6 large-8 columns">
								<div class="acf-map contact-map" data-equalizer-watch>
										<?php 
		
										while($locations->have_posts()) : 

											$locations->the_post();	
											$loc_arr = array(
												'title'=>get_the_title(),
												'id'=>get_the_ID(),
												'phone_number'=>get_field('phone_number'),
												'street_address'=>get_field('street_address'),
												'street_address_two'=>get_field('street_address_two'),
												'city'=>get_field('city'),
												'state'=>get_field('state'),
												'zipcode'=>get_field('zipcode'),
												'url'=>get_the_permalink(),
											);
											$locations_arr[] = $loc_arr;
											$location = get_field('location');
											?>

												<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
													<div class="map-marker">
														<h4><?php the_title() ?> Location</h4>
														<address>
															<div class="address-one">
																<?php the_field('street_address') ?>
															</div>
															<?php if(the_field('street_address_two')): ?>
																<div class="address-two">
																	<?php the_field('street_address_two') ?>
																</div>
															<?php endif; ?>
															<div class="address-three">
																<?php the_field('city') ?>, <?php the_field('state') ?> <?php the_field('zipcode') ?>
															</div>
														</address>
														<a href="<?php the_permalink() ?>">More Info</a>
													</div>
													
												</div>

										<?php
										endwhile;
										?>
								</div><!-- map -->							
							</div>
							<div class="medium-6 large-4 columns text-center" data-equalizer-watch>
								<div class="row tb-pad-90">
									<div class="large-12 columns">
										<h1 class="phone-number"><?php echo do_shortcode('[frn_phone]');?></h1>
										<?php /*removed 2/17/19 due to call center no longer being available 24/7 <p class="tb-pad-20">We're here to help 24 hours, <br>7 days a week</p>*/ ?>
										<div class="row">
											<div class="large-6 medium-10 columns medium-centered">
												<?php echo do_shortcode('[lhn_inpage button="email" text="Email Us" class="button hollow large expanded" ]'); ?>
<?php 
/* 
//The following is the original version before the FRN Plugin used to handle text versions of buttons.
<a onclick="window.open('http://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160','Ticket','left=' + (screen.width - 550-32) / 2 + ',top=50,scrollbars=yes,menubar=no,height=550,width=450,resizable=yes,toolbar=no,location=no,status=no');return false;" href="http://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160" rel="external" target="_blank" class="button hollow large expanded">Email Us</a> 
*/ 
?>
												<?php echo do_shortcode('[lhn_inpage button="chat" text="Chat With Us" offline="remove" class="chat-launch button hollow large expanded" ]'); ?>

<?php /*
//Old code using their on/off triggering of the chat
//New FRN Plugin has it's own that better affects all sites in our network.
if(is_chat_live()): ?>
<a href="#" id="snChat" class="chat-launch button hollow large expanded">Chat With Us</a><br>
<?php endif; 
*/ ?>
											</div>
										</div>
									
									
										<?php wp_link_pages(); ?>
										<?php 
											// only show edit button if user has permission to edit posts
											if( $user_level > 0 ) { 
										?>
											<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post"><i class="icon-pencil icon-white"></i> <?php _e("Edit post","wpbootstrap"); ?></a>
										<?php } ?>
									</div>
								</div>
							</div>

						</div><!-- expanded row -->
							<?php wp_reset_postdata(); ?>
						<?php 
					endif;
									
									
?>
						
						<?php echo get_resources_block(); ?>
						
						
				
					</div> <!-- end #main -->
					
					
					<div id="services-section">
						<div class="row expanded columns medium-up-3">
							<?php $locations_arr = array_slice($locations_arr, 0, 3); ?>
							<?php foreach($locations_arr as $loc):?>
								<div class="column text-center">
									
									<div class="">
										<img src="<?php echo get_template_directory_uri()  ?>/images/map-marker.png" alt="<?php echo $loc['title']?>">
									</div>
									<h3><?php echo $loc['title']?></h3>
									<div class="content-callout-description">
										<div class="row">
											<div class="large-8 columns large-centered">
												<p>
													<?php echo $loc['street_address']; 
															echo (!empty($loc['street_address_two'])) ? ', <span style="white-space: nowrap;">'.$loc['street_address_two'].'</span><br>' : '<br>' ; ?>
													<?php echo $loc['city'].', '.$loc['state'].' '.$loc['zipcode']?>
												</p>
											</div>
										</div>
									</div>
									<a href="<?php echo $loc['url']?>" class="button hollow secondary">Learn More</a>
								</div>
							<?php endforeach; ?>

						</div>
					</div>
					<div id="video-section">
						<div class="row">
							<div class="small-12 columns">
								<h2 class="white-color text-center">
									What to expect when you call
								</h2>
							</div>
						</div>	
						<div class="row">
							<div class="medium-3 columns">
								
							</div>
							<div class="medium-6 medium-offset-3 columns">
								<div class="flex-video widescreen">
									<iframe src="https://www.youtube-nocookie.com/embed/vpsoYJ7tBVc?rel=0&amp;showinfo=0" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
									<!--OLD: <iframe src="https://www.youtube.com/embed/vpsoYJ7tBVc" frameborder="0" allowfullscreen></iframe>-->
								</div>								
							</div>
							<div class="medium-3 columns">
								
							</div>
						</div>
					</div>					
					
					<?php
					//insurance content type
					$args = array(
						'post_type'=>'insurance',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$insurances = new WP_Query( $args );
					if($insurances->have_posts()) : 
					?>
						<div id="insurance-section" class="tb-pad-90 contact-page-ins">
							<div class="row">
								<div class="small-12 columns">
									<h2 class="white-color text-center">
										We Accept These Insurances and More
									</h2>
								</div>
							</div>
							<div class="tb-pad-20">
								<div class="row small-up-2 medium-up-2 large-up-4">
									<?php 
									while($insurances->have_posts()) : 
										$insurances->the_post();	
										$title = get_the_title();
										$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
										?>
										<div class="column" style="padding-bottom: 20px;">
											<img class="insurance-logo" alt="<?php echo $title; ?>" src="<?php echo $feat_image ?>"/>
										</div>
										<?php
										
										
									endwhile;
									?>
								</div><!-- end row -->
							</div>
							<div class="row">
								<div class="large-12 columns text-center"><a href="<?php echo site_url(); ?>/admissions/the-abcs-of-insurance-coverage/" class="button hollow">Learn More</a></div>
							</div>
						</div>

						<?php wp_reset_postdata(); ?>


					<?php endif; ?>
		 
			
			<?php endwhile; ?>		
					
			<?php endif; ?>

<?php get_footer(); ?>