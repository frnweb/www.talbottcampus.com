				<div id="sidebar1" class="large-3 columns sidebar-one float-right" role="complementary" data-equalizer-watch="outer">
					<div id="sidebar-link-box">
						<div id="sidebar-menu-icon">
							<a href="#">
								<span class="spacer"></span>
								<span class="words"> <span id="word-change">More</span> Resources</span>
								</a>
						</div>
						<?php if ( is_single() ):?>
						<div id="mobile-share-shell" class="share-shell">
							<?php echo do_shortcode('[frn_social type="SHARETHIS" ]') ?>
						</div>
						<?php endif; ?>
							<?php if ( is_single() ):?>
							<div class="share-shell">
								<?php echo do_shortcode('[frn_social type="SHARETHIS" ]') ?>
							</div>
							<?php endif; ?>
						<form action="<?php echo home_url( '/' ); ?>" method="get" class="form-inline search-form show-for-large" id="sidebar-search">
							<div class="input-group">
								<span class="input-group-label show-for-lt-ie9">Search</span>
								
								<input class="input-group-field form-control" name="s" id="search" placeholder="<?php _e("Search Resources","wpbootstrap"); ?>" value="" type="text" style="background-color: #FFFFFF; box-shadow: none; border: none;">
								<div class="input-group-button">
									<button type="submit" class="button" style="border: 10px solid #fff; padding: 3px 10px; margin-left: -6px; background-color: #ececec; color: #171e3f;"><?php _e("Go","wpbootstrap"); ?></button>
								</div>
							</div>
						</form>
						<div id="sidebar-menu-items">
							<div class="custom-categories">
							<?php 
								$category_arr = prepare_sidebar_posts();
							?>
								<ul>
								<?php 
								foreach($category_arr as $c){
									?><li><?php 
										$cat_name =  $c['category']->cat_name;
										$cat_url = esc_url( get_category_link( $c['category']->term_id ) );
										?><a href="<?php echo $cat_url ?>"><?php echo $cat_name ?></a><?php 
										?><ul><?php 
										foreach($c['posts'] as $p){
											$title = apply_filters('the_title',$p->post_title);
											$post_url = get_permalink($p->ID);
											?><li><?php
												?><a href="<?php echo $post_url ?>"><?php echo $title ?></a><?php 
											?></li><?php 
										}
										?></ul><?php 
									?></li><?php 
								}
								?>	
								</ul>
							</div>
							<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

								<?php dynamic_sidebar( 'sidebar1' ); ?>

							<?php else : ?>

								<!-- This content shows up if there are no widgets defined in the backend. -->
								
								<div class="alert alert-message">
								
									<p><?php /*_e("Please activate some Widgets","wpbootstrap");*/ ?></p>
								
								</div>

							<?php endif; ?>
						</div>
					</div>


				</div>