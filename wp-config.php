<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Yaogp/AK7ZS3iluSHlELVvaPRlgk1sT9x3CEmGn7bermGFrwHKzqRR3tg9pA9SE2ULigM8TEO4+DFElDvFMutw==');
define('SECURE_AUTH_KEY',  'fEHOyNRbPHPTMAn2TLG0cE8GpEAvVCQenm1CQrJNPlcfULqifO5xmB0qnmZhyYI5UxNnF6A03Ggncke2LtFReg==');
define('LOGGED_IN_KEY',    '8xNCT4ZAXPlXkOnlLa3bPskpiwgujO/pUSoUkGgzxNqA8npfUtIDw7Ra3d5KLGJ5l48o5jQlwlIR4wNE3fjzfw==');
define('NONCE_KEY',        'q8s5kRfKtxcNWDOQiaMnjnvw1vSGAQ/8Op398opJIgeQWuB1JzV53yWwdwmDEHMvdYILxI3FMahv06CYLmJUMg==');
define('AUTH_SALT',        'QFDQzgqmrbQcRzj1nWigUx6feZQ5ZOpyMOQhmPI3XZ75LqOoPYstvAwYe/VCcW5DINjGxqkRovw7NSRUcHEH+g==');
define('SECURE_AUTH_SALT', 'Zp+nL0ze4LT89LspIaRoYtugD+ONmj3Kkr7l43Ou7Gnv5d/XGbecIlrkDFyUxSUkWFUKPm/k/roFkHYXe/YcSQ==');
define('LOGGED_IN_SALT',   'dJ9oraqWgfcz4LmaZ03mM2JBgvt+fPdsqsxg1KrG0i2E0HH/9q1+dHyK6ILPh/S83IVx27K3SI5QVKrMrJihqg==');
define('NONCE_SALT',       '4d/PWoUQRF+DtEhTtoAshtz3jtnrelrEWQUy7WWn3MC7HEZhEzcRgqNBa2tbBvTFuGB8G2VxCDVJv5jw+V5uyw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_kyf3ajva1g_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
