
<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="robots" content="noindex">
  <title>Talbott Recovery</title>
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:700|Source+Sans+Pro:400,700" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/app.css">
  <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="assets/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="assets/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  
    <!-- VOB reCAPTCHA Button Disabler-->
    <script>
      function recaptcha_callback(){
      $('.button--secure').prop("disabled", false);
    };
    </script> 
  
    <!-- VOB reCAPTCHA CDN -->
    <script src="https://www.google.com/recaptcha/api.js"></script>
  
      <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '402262266824056');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=402262266824056&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
  
    <!-- Start Visual Website Optimizer Asynchronous Code -->
  <script type='text/javascript'>
  var _vwo_code=(function(){
  var account_id=341821,
  settings_tolerance=2000,
  library_tolerance=2500,
  use_existing_jquery=false,
  /* DO NOT EDIT BELOW THIS LINE */
  f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
  </script>
  <!-- End Visual Website Optimizer Asynchronous Code -->
  
    <!-- UET Tracking Code for Bing -->
    <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5996387"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
    <!-- End UET Tracking Code for Bing -->
  
      <!-- Bing Click Event Tracking -->
    <script>
    /* VOB CLICK */
    function vobClick() {
      window.uetq = window.uetq || []; 
      window.uetq.push ('event', 'VOB Click', {event_category: 'VOB'});
    }
  
    /* CHAT CLICK */
    function livechatClick() {
      window.uetq = window.uetq || []; 
      window.uetq.push ('event', 'Chat Click', {event_category: 'Live Chat'});
    }  
    </script>
    <!-- End Bing Click Event Tracking -->
  
    <!-- VWO Infinity Callback -->
  <script type="text/javascript">
  var _ictVwoIntegration = function(key) {
      if (key !== undefined && _ictt !== undefined && _vis_opt_readCookie != undefined) {
          var vwoCombination = _vis_opt_readCookie('_vis_opt_exp_'+window[key]+'_combi');
  
          if (typeof _vwo_exp[window[key]] != 'undefined' && typeof(_vwo_exp[window[key]].combination_chosen) != 'undefined') {
              vwoCombination = _vwo_exp[window[key]].combination_chosen;
          }
          if (vwoCombination !== '' && vwoCombination !== null) {
              _ictt.push([ '_setCustomVar', ['vwocomb', vwoCombination] ]);
          }
          _ictt.push([ '_setCustomVar', ['vwoexp', window[key]] ]);
      };
  }
  </script>
  <!-- VWO Infinity Callback -->
  
  <!-- Infinity Tracking Code v2.0
   Copyright Infinity 2019
   www.infinity.co -->
  <script type="text/javascript">
      var _ictt = _ictt || [];
      _ictt.push(['_setIgrp','2728']); // Installation ID
      _ictt.push(['_enableGAIntegration',{'gua':true,'ga':false}]);
      _ictt.push(['_includeExternal',[{'from':'_vis_opt_experiment_id','to':'_ictVwoIntegration'}]]);
      _ictt.push(['_enableAutoDiscovery']);
      _ictt.push(['_track']);
  (function() {
      var ict = document.createElement('script'); ict.type = 'text/javascript'; ict.async = true;
      ict.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'ict.infinity-tracking.net/js/nas.v1.min.js';
      var scr = document.getElementsByTagName('script')[0]; scr.parentNode.insertBefore(ict, scr);
  })();
  </script>
  <!-- Infinity Tracking Code End --></head>

<body>

      
      <div id="nav">            
          <div id="mobilephone" class="grid-x show-for-small-only">
              <a class="cell small-6 mobilephone__chat" data-chat data-chat-offline-text="Email Us" data-chat-online-text="Live Chat" data-ga-event data-ga-category="Live Chat" data-ga-action="Chat Button" data-ga-label="Live Chat Mobile" onClick="livechatClick(); _ictt.push(['Live Chat']);">Live Chat</a>
              <a href="tel:888-418-9657" class="InfinityNumber cell small-6 mobilephone__call" data-ga-event data-ga-category="Phone Numbers" data-ga-action="Phone Clicks" data-ga-label="Phone Clicks Mobile" data-ict-discovery-number="888-418-9657" data-ict-silent-replacements="false">888-418-9657</a>
          </div>
  
          <div class="title-bar show-for-small-only" data-responsive-toggle="mobile-menu" data-hide-for="medium">
              <a href="/lp/" class="title-bar-left">
                  <img src="assets/img/logos/talbott-white.svg" class="site-logo" alt="Talbott Recovery Logo" />
              </a>
  
              <div class="title-bar-right">
                  <button type="button" id="hamburger-menu" class="hamburger nav-button hamburger--squeeze" data-toggle="mobile-menu" data-ga-event data-ga-category="Hambuger Menu" data-ga-action="Navigate" data-ga-label="open mobile menu">
                      <span class="hamburger-box">
                          <span class="hamburger-inner"></span>
                      </span>
                      <span class="hamburger-label">Menu</span>
                  </button>
              </div>
          </div>
  
          <div id="mobile-menu" class="hide-for-medium">
              <ul class="main-menu" data-smooth-scroll>
                  <li class="menu-text">
                          <a href="#treatment" id="mobile-menu-Treatment" data-ga-event data-ga-category="Menu Item" data-ga-action="Navigate" data-ga-label="Treatment">Treatment</a>
                  </li>
                  <li class="menu-text">
                          <a href="#locations" id="mobile-menu-Locations" data-ga-event data-ga-category="Menu Item" data-ga-action="Navigate" data-ga-label="Locations">Locations</a>
                  </li>
                  <li class="menu-text">
                          <a href="#programs" id="mobile-menu-Programs" data-ga-event data-ga-category="Menu Item" data-ga-action="Navigate" data-ga-label="Programs">Programs</a>
                  </li>
                  <li class="menu-text">
                          <a href="#insurance" id="mobile-menu-Insurance" data-ga-event data-ga-category="Menu Item" data-ga-action="Navigate" data-ga-label="Insurance">Insurance</a>
                  </li>
                  <li class="menu-text">
                          <a href="#contact" id="mobile-menu-Contact" data-ga-event data-ga-category="Menu Item" data-ga-action="Navigate" data-ga-label="Contact">Contact</a>
                  </li>
              </ul>
          </div>
  
          <div class="top-bar show-for-medium" id="responsive-menu">
              <div class="inner">
                  <div class="top-bar-left">
                      <a id="nav-logo" href="/lp/">
                          <img src="assets/img/logos/talbott-white.svg" class="site-logo" alt="Talbott Recovery Logo" />
                      </a>
                  </div>
                   <div class="top-bar-right ">
                          <ul id="nav-items" class="dropdown menu" data-smooth-scroll>
                                  <li class="menu-text">
                                          <a href="#treatment" id="menu-Treatment" data-ga-event data-ga-category="Menu Item" data-ga-action="Navigate" data-ga-label="Treatment">Treatment</a>
                                  </li>
                                  <li class="menu-text">
                                          <a href="#locations" id="menu-Locations" data-ga-event data-ga-category="Menu Item" data-ga-action="Navigate" data-ga-label="Locations">Locations</a>
                                  </li>
                                  <li class="menu-text">
                                          <a href="#programs" id="menu-Programs" data-ga-event data-ga-category="Menu Item" data-ga-action="Navigate" data-ga-label="Programs">Programs</a>
                                  </li>
                                  <li class="menu-text">
                                          <a href="#insurance" id="menu-Insurance" data-ga-event data-ga-category="Menu Item" data-ga-action="Navigate" data-ga-label="Insurance">Insurance</a>
                                  </li>
                                  <li class="menu-text">
                                          <a href="#contact" id="menu-Contact" data-ga-event data-ga-category="Menu Item" data-ga-action="Navigate" data-ga-label="Contact">Contact</a>
                                  </li>
                          </ul>
                  </div>
          </div>
          </div>
  
      </div>
  
  <div class="content">
  	<div id="billboard" class="module duo duo--billboard">
  		<div class="inner">
  			
  			<div class="grid-x">
  				<div class="cell content-cell large-6 medium-6  medium-order-1 small-order-2">
  					<div class="duo--card">
  	
  						<h4>Atlanta, GA</h4>
  						<h1>Talbott Recovery</h1>
  						<div class="divider"></div>
  						<p>Since 1989, Talbott Recovery has built a long, sustained history of providing evidence-based integrated treatment for individuals with co-occurring substance use and mental health disorders. 
  							<br><br>
  						With specialized programs for individuals, licensed professionals, pilots, young adults and families, everything we do is aimed at helping patients develop the tools needed to maintain long-term recovery.</p>
  							<div class="expanded button-group">
  								  <a href="tel:888-418-9657"
  								     class="InfinityNumber button button--primary"
  								     data-ict-discovery-number="888-418-9657"
  								     data-ict-silent-replacements="false"
  								     onClick="fbq('track', 'Lead', {
  								                content_name: 'Talbott PPC', 
  								                content_category: 'Landing Pages',
  								                content_type: 'Phone Clicks'
  								              });"
  								     data-ga-event
  								     data-ga-category="Phone Numbers"
  								     data-ga-action="Phone Clicks"
  								     data-ga-label="888-418-9657">
  								     888-418-9657
  								  </a>
  								
  								
  							</div>
  	
  	
  						
  	
  					</div>
  				</div>
  				<div class="cell media-cell large-6 medium-6  medium-order-2 small-order-1">
  					<div class="duo--media " style="background-image: url(assets/img/gallery/Gallery3.jpg)">
  						
  						
  						<a class="button--video" data-ga-event data-ga-category="Video" data-ga-action="open modal" data-ga-label=" Video" data-open="talbott-video"></a>
  	
  	
  					</div>
  				</div>
  				
  				<div class="reveal video" data-reset-on-close="true" id="talbott-video" data-reveal>
  					<div class="widescreen flex-video">
  						<iframe width="560" height="315" src="https://www.youtube.com/embed/5YQ6NP1eDgc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
  					</div>
  	          		<button class="close-button" data-close aria-label="Close modal" type="button">
  	            		<span aria-hidden>&times;</span>
  	          		</button>
  				</div>
  	
  			</div>
  		</div>
  	</div>	<div id="treatment" class="module deck ">
  		<div class="inner expanded">
  			<div class="grid-x medium-up-2 large-up-4">
  				<div class="cell">
  					<a class="deck__card" data-ga-event data-ga-category="deck-cards" data-ga-action="open modal" data-ga-label="Detox and  
  	Stabilization
  	" data-open="deck-item-0">
  						<div class="deck__media" style="background-image: url(assets/img/treatment/DetoxStabilization.jpg)"></div>
  						<div class="deck__content">
  							<p>Detox and<br>Stabilization</p>
  	
  							<div class="button button--outline">Read More</div>
  						</div>
  					</a>
  				</div>
  	
  				<div class="reveal deck-modals" id="deck-item-0" data-reveal>
  					<div class="reveal-card">
  	            		<h2>Detox and  
  	Stabilization
  	</h2>
  	            		<p>When a person struggling with an addiction decides to seek treatment, we will do a comprehensive assessment of the patient’s health to determine the best course of action. Not everyone needs medically supervised detox, but certain addictions may require detox as an entry point to treatment. The type of detox you need depends on many factors - the type of addiction you have, how long you’ve been using, how much you are using, the existence of other addictions or mental health issues, current living conditions, as well as your commitment to sobriety.</p>
  	
  	            		<div class="deck-modals__cta">
  						  <a href="tel:888-418-9657"
  						     class="InfinityNumber button button--primary"
  						     data-ict-discovery-number="888-418-9657"
  						     data-ict-silent-replacements="false"
  						     onClick="fbq('track', 'Lead', {
  						                content_name: 'Talbott PPC', 
  						                content_category: 'Landing Pages',
  						                content_type: 'Phone Clicks'
  						              });"
  						     data-ga-event
  						     data-ga-category="Phone Numbers"
  						     data-ga-action="Phone Clicks"
  						     data-ga-label="888-418-9657">
  						     888-418-9657
  						  </a>
  						
  						
  						<h4>Ask us about our Detox and  
  	Stabilization
  	 programs</h4>
  	            		</div>
  	          		</div>
  	          		<button class="close-button" data-close aria-label="Close modal" type="button">
  	            		<span aria-hidden>&times;</span>
  	          		</button>
  				</div>
  				<div class="cell">
  					<a class="deck__card" data-ga-event data-ga-category="deck-cards" data-ga-action="open modal" data-ga-label="Partial  
  	Hospitalization
  	" data-open="deck-item-1">
  						<div class="deck__media" style="background-image: url(assets/img/treatment/PartialHospitalization.jpg)"></div>
  						<div class="deck__content">
  							<p>Partial<br>Hospitalization</p>
  	
  							<div class="button button--outline">Read More</div>
  						</div>
  					</a>
  				</div>
  	
  				<div class="reveal deck-modals" id="deck-item-1" data-reveal>
  					<div class="reveal-card">
  	            		<h2>Partial  
  	Hospitalization
  	</h2>
  	            		<p>Talbott Recovery’s main campus offers a Partial Hospitalization Program (PHP), which is an option in-between residential treatment and an outpatient program. This allows patients to have private time and reflection as well as the opportunity to process the new ways of living, thinking and feeling that accompany addiction recovery – all within close proximity to our experienced treatment professionals.</p>
  	
  	            		<div class="deck-modals__cta">
  						  <a href="tel:888-418-9657"
  						     class="InfinityNumber button button--primary"
  						     data-ict-discovery-number="888-418-9657"
  						     data-ict-silent-replacements="false"
  						     onClick="fbq('track', 'Lead', {
  						                content_name: 'Talbott PPC', 
  						                content_category: 'Landing Pages',
  						                content_type: 'Phone Clicks'
  						              });"
  						     data-ga-event
  						     data-ga-category="Phone Numbers"
  						     data-ga-action="Phone Clicks"
  						     data-ga-label="888-418-9657">
  						     888-418-9657
  						  </a>
  						
  						
  						<h4>Ask us about our Partial  
  	Hospitalization
  	 programs</h4>
  	            		</div>
  	          		</div>
  	          		<button class="close-button" data-close aria-label="Close modal" type="button">
  	            		<span aria-hidden>&times;</span>
  	          		</button>
  				</div>
  				<div class="cell">
  					<a class="deck__card" data-ga-event data-ga-category="deck-cards" data-ga-action="open modal" data-ga-label="Integrated  
  	Treatment
  	" data-open="deck-item-2">
  						<div class="deck__media" style="background-image: url(assets/img/treatment/Integrated-Treatment-Man-Smiling.jpg)"></div>
  						<div class="deck__content">
  							<p>Integrated<br>Treatment</p>
  	
  							<div class="button button--outline">Read More</div>
  						</div>
  					</a>
  				</div>
  	
  				<div class="reveal deck-modals" id="deck-item-2" data-reveal>
  					<div class="reveal-card">
  	            		<h2>Integrated  
  	Treatment
  	</h2>
  	            		<p>Mental disorders and addiction often go hand in hand. According to the National Institute on Drug Abuse (NIDA), people who are diagnosed with a mood or anxiety disorder are twice as likely to struggle with addiction. When co-occurring addiction and mental health issues such as depression, anxiety, bipolar disorder and psychological trauma are treated concurrently, individuals are much more likely to achieve and sustain a healthy, balanced lifestyle. Talbott Recovery offers integrated treatment using evidence-based methods for the best chance of long-term success in recovery.</p>
  	
  	            		<div class="deck-modals__cta">
  						  <a href="tel:888-418-9657"
  						     class="InfinityNumber button button--primary"
  						     data-ict-discovery-number="888-418-9657"
  						     data-ict-silent-replacements="false"
  						     onClick="fbq('track', 'Lead', {
  						                content_name: 'Talbott PPC', 
  						                content_category: 'Landing Pages',
  						                content_type: 'Phone Clicks'
  						              });"
  						     data-ga-event
  						     data-ga-category="Phone Numbers"
  						     data-ga-action="Phone Clicks"
  						     data-ga-label="888-418-9657">
  						     888-418-9657
  						  </a>
  						
  						
  						<h4>Ask us about our Integrated  
  	Treatment
  	 programs</h4>
  	            		</div>
  	          		</div>
  	          		<button class="close-button" data-close aria-label="Close modal" type="button">
  	            		<span aria-hidden>&times;</span>
  	          		</button>
  				</div>
  				<div class="cell">
  					<a class="deck__card" data-ga-event data-ga-category="deck-cards" data-ga-action="open modal" data-ga-label="Outpatient  
  	Programs
  	" data-open="deck-item-3">
  						<div class="deck__media" style="background-image: url(assets/img/treatment/IntensiveOutpatientProgram.jpg)"></div>
  						<div class="deck__content">
  							<p>Outpatient<br>Programs</p>
  	
  							<div class="button button--outline">Read More</div>
  						</div>
  					</a>
  				</div>
  	
  				<div class="reveal deck-modals" id="deck-item-3" data-reveal>
  					<div class="reveal-card">
  	            		<h2>Outpatient  
  	Programs
  	</h2>
  	            		<p>If you’ve been struggling with an alcohol or drug addiction alongside a co-occurring psychiatric disorder, you know the negative impact it can have on your life. There are treatment options that don’t require inpatient status and allow you to still participate in daily activities like work and childcare. Talbott Recovery offers nonresidential, outpatient addiction treatment programs to provide professional, compassionate, private outpatient addiction treatment that can give you hope and provide you with the tools to find a better way to live. These programs allow you to live in your own residence and to visit one of Talbott’s outpatient locations for daily treatment, much like visiting a doctor’s or counselor’s office regularly.</p>
  	
  	            		<div class="deck-modals__cta">
  						  <a href="tel:888-418-9657"
  						     class="InfinityNumber button button--primary"
  						     data-ict-discovery-number="888-418-9657"
  						     data-ict-silent-replacements="false"
  						     onClick="fbq('track', 'Lead', {
  						                content_name: 'Talbott PPC', 
  						                content_category: 'Landing Pages',
  						                content_type: 'Phone Clicks'
  						              });"
  						     data-ga-event
  						     data-ga-category="Phone Numbers"
  						     data-ga-action="Phone Clicks"
  						     data-ga-label="888-418-9657">
  						     888-418-9657
  						  </a>
  						
  						
  						<h4>Ask us about our Outpatient  
  	Programs
  	</h4>
  	            		</div>
  	          		</div>
  	          		<button class="close-button" data-close aria-label="Close modal" type="button">
  	            		<span aria-hidden>&times;</span>
  	          		</button>
  				</div>
  			</div>
  		</div>
  	</div>	<div id="offers" class="module callout ">
  		<div class="inner">
  			<div class="card-section">
  				<div class="grid-x">
  					<div class="cell medium-3">
  						<img src="assets/img/logos/talbott-stacked-color.svg" alt="Talbott Logo" />
  					</div>
  					<div class="cell medium-9">
  						<h3>Talbott Recovery Offers:</h3>
  						<ul>
  								<li>Comprehensive, 72-hour assessments</li>
  								<li>Partial hospitilization with supportive living</li>
  								<li>Family therapy and robust family programming</li>
  								<li>Extensive alumni support services</li>
  								<li>Specialized groups, such as DBT, anger, grief, LGBTQ+, spirituality, gender-specific groups for trauma and gender-specific groups for sexual issues</li>
  								<li>Full-time dual-boarded psychiatrists</li>
  								<li>Psychoeducational groups, life skills training and education on how to use the 12 Steps</li>
  								<li>Medically supervised detox</li>
  						</ul>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>	<div id="locations" class="module location-map " style="background-image: url(assets/img/backgrounds/Locations.jpg)">
  		<div class="inner">
  			<div class="grid-x">
  				<div class="cell large-6 large-order-1 medium-order-2 small-order-2">
  					<div class="map-card">
  						<h2>Talbott Locations</h2>
  						<p>Talbott Recovery’s Atlanta campus is conveniently located to serve Metro Atlanta and a wide range of suburbs. In addition to Talbott Recovery Atlanta, we have two convenient outpatient locations ready to help in Columbus, GA and Dunwoody, GA. </p>
  	
  						<div class="map-address">
  							<h4>Main Campus</h4>
  								<p><strong>Talbott Recovery Atlanta</strong><br>  5355 Hunter Rd<br>  Atlanta, GA 30349</p>
  	
  								<h4>Outpatient Locations</h4>
  									<p><strong>Talbott Recovery Dunwoody</strong><br>2153 Peachford Rd<br>Dunwoody, GA 30338</p>
  	
  									<p><strong>Talbott Recovery Columbus</strong><br>1200 Brookstone Centre Pkwy #210<br>Columbus, GA 31904</p>
  	
  							
  						</div>
  					</div>
  				</div>
  				<div class="cell large-6 large-order-2 medium-order-1 small-order-1">
  					<div class="map-container">
  						<div class="map"
  						data-map
  						data-map-lat="33.6151466"
  						data-map-lng="-84.5456921"
  	        			data-map-zoom="12"
  						data-map-style="grey"></div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>	<div id="gallery" class="module gallery ">
  		<div class="inner">
  			<div class="grid-x gallery-container">
  				<div class="cell large-6 large-order-1 medium-order-1 small-order-1">
  					<div class="facility-card">
  						<h3>Talbott Recovery Atlanta Campus</h3>
  						<p>Talbott Recovery’s campus is located on 200 acres of land featuring a 30-acre spring-fed lake allowing for the opportunity of outdoor therapies. This peaceful area provides a safe environment to help individuals establish a foundation for long-term recovery.
  	
  	Indoors, you&#x27;ll find a beautiful space flooded with natural light and includes a yoga room and a large chapel with stunning views of the surrounding foothills. Every area throughout the campus is intentional and used as a place to heal. Even the lecture hall hosts additional therapies such as music therapy and psychodrama.
  	</p>		
  					</div>
  				</div>
  				<div class="cell large-6 large-order-2 medium-order-2 small-order-2">
  	
  					<div id="gallery-slider" class="gallery__slider">
  						<div class="gallery-image" style="background-image: url(assets/img/gallery/Gallery1.jpg)"></div>
  						<div class="gallery-image" style="background-image: url(assets/img/gallery/Gallery2.jpg)"></div>
  						<div class="gallery-image" style="background-image: url(assets/img/gallery/Gallery3.jpg)"></div>
  						<div class="gallery-image" style="background-image: url(assets/img/gallery/Gallery4.jpg)"></div>
  						<div class="gallery-image" style="background-image: url(assets/img/gallery/Gallery5.jpg)"></div>
  						<div class="gallery-image" style="background-image: url(assets/img/gallery/Gallery6.jpg)"></div>
  					</div>
  	
  				</div>
  			</div>
  		</div>
  	</div>	<div id="programs" class="module programs">
  		<h2 id="programs-header" class="program-header">Programs</h2>
  		<div id="professionals" class="module duo ">
  			<div class="inner">
  				
  				<div class="grid-x">
  					<div class="cell content-cell large-6 medium-6  medium-order-2 small-order-2">
  						<div class="duo--card">
  		
  		
  							<h2>Licensed Professionals Program</h2>
  							<p>For professionals in a variety of fields, getting treatment isn’t always as easy as picking up the phone. There are unique needs that must be addressed. Fortunately, there is hope for professionals dealing with an addiction. With over 30 years of history, we have helped thousands of professionals from all 50 states, Canada and Europe work through their specific needs and get their lives back on track. No one has more experience treating professionals than Talbott Recovery.</p>
  		
  		
  						</div>
  					</div>
  					<div class="cell media-cell large-6 medium-6  medium-order-1 small-order-1">
  						<div class="duo--media " style="background-image: url(assets/img/duos/ProfessionalsProgram.jpg)">
  							
  							
  		
  		
  						</div>
  					</div>
  					
  		
  				</div>
  			</div>
  		</div>		<div id="pilots" class="module duo ">
  			<div class="inner">
  				
  				<div class="grid-x">
  					<div class="cell content-cell large-6 medium-6  medium-order-1 small-order-2">
  						<div class="duo--card">
  		
  		
  							<h2>Pilots Program</h2>
  							<p>Pilots face specific issues that are unique to their profession and must be considered in the design of any addiction treatment plan. Our program addresses not only the medical, psychological and social aspects of the disease of addiction, but it is also the most efficient pathway for a pilot to return to work. The program is directed by senior clinicians who thoroughly understand the disease of addiction and the needs of the pilot, their employer and the FAA.</p>
  		
  		
  						</div>
  					</div>
  					<div class="cell media-cell large-6 medium-6  medium-order-2 small-order-1">
  						<div class="duo--media " style="background-image: url(assets/img/duos/PilotsProgram.jpg)">
  							
  							
  		
  		
  						</div>
  					</div>
  					
  		
  				</div>
  			</div>
  		</div>		<div id="young-adults" class="module duo ">
  			<div class="inner">
  				
  				<div class="grid-x">
  					<div class="cell content-cell large-6 medium-6  medium-order-2 small-order-2">
  						<div class="duo--card">
  		
  		
  							<h2>Young Adults</h2>
  							<p>Most young adults who suffer from addiction have a harder time achieving sobriety than their adult counterparts. Talbott Recovery’s Young Adult Addiction Program was designed to address that difficulty. This comprehensive program addresses not just the medical and physical issues related to addiction, but it also gets to the root of any psychological, psychiatric and spiritual aspects that may contribute to the problem.</p>
  		
  		
  						</div>
  					</div>
  					<div class="cell media-cell large-6 medium-6  medium-order-1 small-order-1">
  						<div class="duo--media " style="background-image: url(assets/img/duos/YoungAdults.jpg)">
  							
  							
  		
  		
  						</div>
  					</div>
  					
  		
  				</div>
  			</div>
  		</div>		<div id="family" class="module duo ">
  			<div class="inner">
  				
  				<div class="grid-x">
  					<div class="cell content-cell large-6 medium-6  medium-order-1 small-order-2">
  						<div class="duo--card">
  		
  		
  							<h2>Family Program</h2>
  							<p>A loved one’s alcohol or drug abuse creates ripples that impact the entire family, and we view family as an essential component of a patient’s ongoing recovery program. Once a patient is admitted to any Talbott Recovery treatment program, a counselor who specializes in addiction and substance abuse family therapy is assigned to respond to any questions or concerns you may have. Our Family Program is designed to assess the family’s needs, educate loved ones about chemical dependence and offer ongoing family support during and after the patient’s treatment.</p>
  		
  		
  						</div>
  					</div>
  					<div class="cell media-cell large-6 medium-6  medium-order-2 small-order-1">
  						<div class="duo--media " style="background-image: url(assets/img/duos/FamilyProgram.jpg)">
  							
  							
  		
  		
  						</div>
  					</div>
  					
  		
  				</div>
  			</div>
  		</div>	</div>
  	<div id="admissions" class="module spotlight spotlight--primary"  >
  		<div class="inner">
  				<div class="spotlight__card">
  					<h3>The Admissions Process Simplified</h3>
  					<p class="spotlight__text">Admitting yourself or your loved one for treatment is a big decision that can feel intimidating. We understand that it can be a challenge to know who you can trust to have your best interest at heart.</p>
  	
  					
  					<h3 class="callout-header">That’s why our admissions team has one simple goal: to provide honest, straightforward, life-changing help.</h3> 
  					<div class="grid-x admissions-steps">
  							<div class="spotlight__callout">
  									<p class="callout-text">When you call, we’ll guide you through a free, confidential assessment to determine if Talbott Recovery is the right fit for your needs. If so, our team will also connect with your insurance company to determine your coverage and any out-of-pocket costs. We do this upfront so that we can help you make the most informed decision possible.</p>
  									<div class="grid-x">
  										<div class="cell medium-8">
  											<p class="callout-cta">If you have questions about what Talbott Recovery has to offer or want to speak with someone about starting the admissions process, please contact us today!</p>
  										</div>
  										<div class="cell medium-4">
  											<div class="expanded button-group">
  												  <a href="tel:888-418-9657"
  												     class="InfinityNumber button button--primary"
  												     data-ict-discovery-number="888-418-9657"
  												     data-ict-silent-replacements="false"
  												     onClick="fbq('track', 'Lead', {
  												                content_name: 'Talbott PPC', 
  												                content_category: 'Landing Pages',
  												                content_type: 'Phone Clicks'
  												              });"
  												     data-ga-event
  												     data-ga-category="Phone Numbers"
  												     data-ga-action="Phone Clicks"
  												     data-ga-label="888-418-9657">
  												     888-418-9657
  												  </a>
  												
  												
  											</div>
  										</div>
  									</div>
  								</div>
  					</div>
  	
  				</div> 
  		</div>
  	</div>	<div id="insurance" class="module duo duo--insurance">
  		<div class="inner">
  			
  			<div class="grid-x">
  				<div class="cell content-cell large-7 medium-6  medium-order-1 small-order-1">
  					<div class="duo--card">
  	
  	
  						<h2>Many Insurance Providers Accepted</h2>
  						<p>When you or your loved one needs treatment, concerns about how much it will cost and who will pay often emerge. Even when you have health insurance, it can be difficult to understand your coverage options.</p>
  	<p>We’re here to help. Call us now so we can give you the information you need. We can even recommend other programs if ours isn’t the best fit for you or your loved one.</p>
  	
  	
  					</div>
  				</div>
  				<div class="cell media-cell large-5 medium-6  medium-order-2 small-order-2">
  					<div class="duo--media " style="background-image: url()">
  						
  						
  	
  						<div class="grid-x small-up-3 large-up-3">
  						<div class="cell">
  							<img src="assets/img/insurance/bluecrossblueshield.png" alt="BlueCross BlueShield" class="insurance-logos">
  						</div>
  						<div class="cell">
  							<img src="assets/img/insurance/healthnet.png" alt="Health Net" class="insurance-logos">
  						</div>
  						<div class="cell">
  							<img src="assets/img/insurance/compsych.png" alt="ComPsych" class="insurance-logos">
  						</div>
  						<div class="cell">
  							<img src="assets/img/insurance/coventry.png" alt="Coventry" class="insurance-logos">
  						</div>
  						<div class="cell">
  							<img src="assets/img/insurance/cigna.png" alt="Cigna" class="insurance-logos">
  						</div>
  						<div class="cell">
  							<img src="assets/img/insurance/aetna.png" alt="Aetna" class="insurance-logos">
  						</div>
  						<div class="cell">
  							<img src="assets/img/insurance/humana.png" alt="Humana" class="insurance-logos">
  						</div>
  						<div class="cell">
  							<img src="assets/img/insurance/valueoptions.png" alt="Value Options" class="insurance-logos">
  						</div>
  						<div class="cell">
  							<img src="assets/img/insurance/tricare.png" alt="TriCare" class="insurance-logos">
  						</div>
  						</div>
  	
  					</div>
  				</div>
  				
  	
  			</div>
  		</div>
  	</div>	<div id="contact" class="module spotlight " style="background-image:url(assets/img/backgrounds/FinalCTA.jpg)" >
  		<div class="inner">
  				<div class="spotlight__card">
  					<h3>Give Us A Call</h3>
  					<p >If you would like to speak with someone about getting help for yourself or a loved one please contact us today. Our admissions coordinators are available to provide a confidential assessment and review the best treatment options for your situation.
  	</p>
  	
  					<h3>Help is here for you.</h3>
  					<h3 class="callout-header"></h3> 
  	
  					<div class="expanded button-group">
  				          <a href="tel:888-418-9657"
  				             class="InfinityNumber button button--primary"
  				             data-ict-discovery-number="888-418-9657"
  				             data-ict-silent-replacements="false"
  				             onClick="fbq('track', 'Lead', {
  				                        content_name: 'Talbott PPC', 
  				                        content_category: 'Landing Pages',
  				                        content_type: 'Phone Clicks'
  				                      });"
  				             data-ga-event
  				             data-ga-category="Phone Numbers"
  				             data-ga-action="Phone Clicks"
  				             data-ga-label="888-418-9657">
  				             888-418-9657
  				          </a>
  				        
  				        
  				    </div>
  				</div> 
  		</div>
  	</div>	<div id="footer" class="footer">
  		<div class="inner">
  			<div class="grid-x small-up-2 medium-up-5 footer__accreditations">
  				<div class="cell">
  					<script src="https://static.legitscript.com/seals/3417926.js"></script>
  				</div>
  				<div class="cell">
  					<img src="assets/img/accreditations/GHA-white.png" alt="FRN is certified by Georgia Hospital Association">
  				</div>
  				<div class="cell">
  					<img src="assets/img/accreditations/ADACBGA.png" alt="FRN is certified by Alcohol and Drug Abuse Certification Board of Georgia">
  				</div>
  				<div class="cell">
  					<img src="assets/img/accreditations/jointcommission.png" alt="FRN is certified by Joint Commission">
  				</div>
  				<div class="cell">
  					<img src="assets/img/accreditations/naatp.png" alt="FRN is certified by NAATP">
  				</div>
  			</div>
  			<div class="privacy-policy">
  		  			Copyright © 2019 Talbott Recovery. All Rights Reserved. | Confidential and Private Call:   <a href="tel:888-418-9657"
  					  class="InfinityNumber"
  					  data-ict-discovery-number="888-418-9657"
  					  data-ict-silent-replacements="false"
  					  onClick="fbq('track', 'Lead', {
  								 content_name: 'Talbott PPC', 
  								 content_category: 'Landing Pages',
  								 content_type: 'Phone Clicks'
  							   });"
  					  data-ga-event
  					  data-ga-category="Phone Numbers"
  					  data-ga-action="Phone Clicks"
  					  data-ga-label="888-418-9657">
  					  888-418-9657
  				   </a> | <a href="https://talbottcampus.com/wp-content/plugins/frn_plugins/privacy-policy.pdf">Privacy Policy</a><br>
  				   <p class="commission-call">If you have comments or concerns and want to speak with the Joint Commission, you can contact their Customer Service line at <a href="tel:630-792-5800">630-792-5800</a>.</p>
  	    	</div>
  		</div>
  	</div>
  	<div class="reveal vob" id="vob-form" data-reveal>
  	  <div class="grid-container full" id="vob-holder">
  	          <div class="brand_block"></div><!-- end logo for that site -->
  	                  <!---form stuff will go here--->
  	                      <form  action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding&#x3D;UTF-8" method="POST"> 
  	                          <input type="hidden" name='captcha_settings' value='{"keyname":"FRNCaptcha","fallback":"true","orgId":"00Dj0000001rMuh","ts":""}'/> 
  	                          <input type="hidden" name="oid" value="00Dj0000001rMuh">
  	                          <input type="hidden" name="retURL" value="http://talbottcampus.com/lp/vob-thank-you.html">    
  	                          
  	                          <div class="inner">  
  	                              <h3>Let’s Get You Some Answers</h3>
  	                              <p><p> By requesting a <strong>risk-free, confidential assessment</strong> we promise to guide you in finding the best solution for your individual needs.</p>
  	                   </p>
  	                           
  	                              <div class="grid-x grid-margin-x">
  	                                  <div class="large-12 medium-12 cell">
  	                                      <h5>Who are you seeking help for today?</h5>
  	                                  </div><!-- end cell --> 
  	                  
  	                                  <div class="large-6 medium-6 cell">
  	                                      <select id="00Nj000000BKJje" name="00Nj000000BKJje" title="Calling for" required>
  	                                          <option value="" disabled selected>Select One</option>
  	                                          <option value="Self">Myself</option>
  	                                          <option value="Loved One">Loved One</option>
  	                                          <option value="Client/Patient">Client/Patient</option>
  	                                      </select><!-- end calling for dropdown-->
  	                                   </div><!-- end cell -->
  	                              </div> <!---end row --> 
  	                  
  	                              <div class="grid-x grid-margin-x">
  	                                  <div class="large-12 medium-12 cell"> 
  	                                      <h5>Is the person in need of help 18 or older?</h5>
  	                                      <input  id="00N0a00000CVU7J" name="00N0a00000CVU7J" type="checkbox" value="1" /><label for="00N0a00000CVU7J">Yes</label>
  	                                      <input  id="under_eighteen" name="under_eighteen" type="checkbox" value="No" /><label for="under_eighteen">No</label>
  	                                  </div><!-- end cell -->
  	                              </div><!---end row --> <!-- end section about who they are reaching out for--> 
  	                  
  	                              <hr>
  	                  
  	                              <div class="grid-x grid-margin-x">
  	                                  <div class="large-6 medium-6 cell">
  	                                      <label for="first_name"><h5><span class="patient-reveal">Patient</span> First Name</h5></label>
  	                                      <input id="first_name" maxlength="40" name="first_name" type="text" required>
  	                                  </div><!-- end cell for patient first name -->
  	                  
  	                                  <div class="large-6 medium-6 cell">
  	                                      <label for="last_name"><h5><span class="patient-reveal">Patient</span> Last Name</h5></label>
  	                                      <input  id="last_name" maxlength="80" name="last_name" type="text" required>
  	                                  </div><!-- end cell for patient last name-->
  	                              </div><!-- end grid row -->
  	                  
  	                             <div id="caller-contact" class="patient-reveal">
  	                                   <div class="grid-x grid-margin-x">
  	                                      <div class="large-6 medium-6 cell">
  	                                          <label for="00Nj000000BKJjY"><h5>Contact First Name</h5></label>
  	                                          <input  id="00Nj000000BKJjY" maxlength="72" name="00Nj000000BKJjY" type="text" />
  	                                        </div><!-- end cell for Contact first name-->
  	                  
  	                                      <div class="large-6 medium-6 cell">
  	                                          <label for="00Nj000000BKJja"><h5>Contact Last Name</h5></label>
  	                                          <input  id="00Nj000000BKJja" maxlength="72" name="00Nj000000BKJja" type="text" /><!-- caller last name-->
  	                                        </div><!-- end large-12-->
  	                                     </div><!-- end grid row -->
  	                                </div><!-- end caller-contact -->
  	                  
  	                              <div class="grid-x grid-margin-x">
  	                                <div class="large-6 medium-6 cell">
  	                                      <label for="phone"><h5><span class="contact-reveal">Contact</span> Phone Number</h5></label>
  	                                      <input id="phone" maxlength="40" name="phone" type="text" placeholder="XXX-XXX-XXXX" required>
  	                                </div><!-- contact phone number-->
  	                  
  	                                 <div class="large-6 medium-6 cell">
  	                                      <label for="email"><h5><span class="contact-reveal">Contact</span> Email <span class="optional">- Optional</span></h5></label>
  	                                      <input id="email" maxlength="80" name="email" type="text">
  	                                </div><!-- end email-->
  	                              </div><!-- end grid row -->
  	                  
  	                             <div class="grid-x grid-margin-x">
  	                                  <div class="large-6 medium-6 cell">
  	                                      <label for="city"><h5><span class="patient-reveal">Patient</span> City</h5></label><input id="city" maxlength="40" name="city" size="20" type="text" required/>
  	                                  </div><!-- end cell for patient city-->
  	                  
  	                                  <div class="large-6 medium-6 cell">
  	                                      <label for="state_code">
  	                                          <h5><span class="patient-reveal">Patient</span> State</h5></label>
  	                                          <select  id="state_code" name="state_code" required>
  	                                              <option value="AL">AL</option>
  	                                              <option value="AK">AK</option>
  	                                              <option value="AZ">AZ</option>
  	                                              <option value="AR">AR</option>
  	                                              <option value="CA">CA</option>
  	                                              <option value="CO">CO</option>
  	                                              <option value="CT">CT</option>
  	                                              <option value="DE">DE</option>
  	                                              <option value="FL">FL</option>
  	                                              <option value="GA">GA</option>
  	                                              <option value="HI">HI</option>
  	                                              <option value="ID">ID</option>
  	                                              <option value="IL">IL</option>
  	                                              <option value="IN">IN</option>
  	                                              <option value="IA">IA</option>
  	                                              <option value="KS">KS</option>
  	                                              <option value="KY">KY</option>
  	                                              <option value="KY">LA</option>
  	                                              <option value="ME">ME</option>
  	                                              <option value="MD">MD</option>
  	                                              <option value="MA">MA</option>
  	                                              <option value="MI">MI</option>
  	                                              <option value="MN">MN</option>
  	                                              <option value="MS">MS</option>
  	                                              <option value="MO">MO</option>
  	                                              <option value="MT">MT</option>
  	                                              <option value="NE">NE</option>
  	                                              <option value="NV">NV</option>
  	                                              <option value="NH">NH</option>
  	                                              <option value="NJ">NJ</option>
  	                                              <option value="NM">NM</option>
  	                                              <option value="NY">NY</option>
  	                                              <option value="NC">NC</option>
  	                                              <option value="ND">ND</option>
  	                                              <option value="NO">NO</option>
  	                                              <option value="OH">OH</option>
  	                                              <option value="OK">OK</option>
  	                                              <option value="OR">OR</option>
  	                                              <option value="PA">PA</option>
  	                                              <option value="RI">RI</option>
  	                                              <option value="SC">SC</option>
  	                                              <option value="SD">SD</option>
  	                                              <option value="TN">TN</option>
  	                                              <option value="TX">TX</option>
  	                                              <option value="UT">UT</option>
  	                                              <option value="VA">VA</option>
  	                                              <option value="VT">VT</option>
  	                                              <option value="WA">WA</option>
  	                                              <option value="WV">WV</option>
  	                                              <option value="WI">WI</option>
  	                                              <option value="WY">WY</option>
  	                                          </select><!-- end drop down for states -->
  	                                </div><!-- end cell for state dropdown-->
  	                             </div><!-- end grid row for patient state and city-->
  	                  
  	                             <div class="grid-x grid-margin-x">   
  	                                    <div class="large-12 medium-12 cell">
  	                                      <label for="00N0a00000CVU7I"><h5><span class="patient-reveal">Patient</span> Insurance Type</h5></label>
  	                                      <select id="00N0a00000CVU7I" name="00N0a00000CVU7I" title="Insurance Type" required>
  	                                          <option value="" disabled selected>View Options</option>
  	                                          <option value="Employer Provided/Individually Provided">Employer Provided/Individually Provided</option>
  	                                          <option value="Government Provided (Medicaid)">Government Provided (Medicaid)</option>
  	                                          <option value="Government Provided (Medicare)">Government Provided (Medicare)</option>
  	                                          <option value="No Insurance Cash Pay">No Insurance Cash Pay</option>
  	                                          <option value="No Insurance, Will Need Financial Assistance">No Insurance, Will Need Financial Assistance</option>
  	                                      </select>
  	                                    </div><!-- end type of insurance -->
  	                             </div><!--end row for insurance type-->    
  	                  
  	                          </div><!-- end inner -->
  	                              
  	                          <div class="alt inner">
  	                              <h5 class="clock-icon">Want to speed up the process? <span class="optional">- Optional</span></h5>
  	                              <p>If you can provide a little more information now, we&#x27;ll go ahead and get to work on finding personalized options based on your situation and coverage.</p> 
  	                  
  	                              <div id="speed-up-fields">
  	                                  <input type="radio" name="speed-up" id="speed-up-yes" checked><label for="speed-up-yes">Yes</label>
  	                                  <input type="radio" name="speed-up" id="speed-up-no"><label for="speed-up-no">No thanks</label>
  	                                  <div id="additional-fields">
  	                                      <div class="grid-x grid-margin-x">
  	                                          <div class="large-12 cell">
  	                                              <label for="00N0a00000CVU7F">
  	                                                  <h5><span class="patient-reveal">Patient</span> Insurance Company <span class="optional">- Optional</span><span data-tooltip tabindex="1" title="" data-tooltip-class="tooltip insurance-company"><img src="assets/img/vob/question-circle.png" width="15" height="15" class="form-tool-tip"></span></h5>
  	                                              </label> 
  	                                              <input id="00N0a00000CVU7F" maxlength="255" name="00N0a00000CVU7F" type="text">
  	                                          </div><!-- end cell for the insurance company name-->
  	                  
  	                                          <div class="large-12 cell">
  	                                              <label for="00N0a00000CVU7G">
  	                                                  <h5><span class="patient-reveal">Patient</span> Insurance ID Number <span class="optional">- Optional</span><span data-tooltip tabindex="1" title="" data-tooltip-class="tooltip insurance-id"><img src="assets/img/vob/question-circle.png" width="15" height="15" class="form-tool-tip"></span> </h5>
  	                                              </label>
  	                                              <input  id="00N0a00000CVU7G" maxlength="50" name="00N0a00000CVU7G" type="text">
  	                                            </div><!-- end cell for the insurance ID number-->
  	                   
  	                                          <div class="large-12 cell">
  	                                              <label for="00N0a00000CVU7H">
  	                                                  <h5><span class="patient-reveal">Patient</span> Insurance Phone Number <span class="optional">- Optional</span><span data-tooltip tabindex="1" title="" data-tooltip-class="tooltip insurance-phone"><img src="assets/img/vob/question-circle.png" width="15" height="15" class="form-tool-tip"></span></h5>
  	                                              </label> 
  	                                              <input id="00N0a00000CVU7H" maxlength="50" name="00N0a00000CVU7H" type="text">
  	                                            </div><!-- end large-12-->
  	                  
  	                                          <div class="large-6 medium-6 cell">
  	                                                  <label for="00Nj000000BKJjV">
  	                                                  <h5><span class="patient-reveal">Patient</span> Date of Birth <span class="optional">- Optional</span></h5></label><span class="dateInput dateOnlyInput"><input  id="00Nj000000BKJjV" name="00Nj000000BKJjV" type="text" placeholder="MM/DD/YYYY" /></span>
  	                                          </div><!-- end cell for the DOB-->
  	                  
  	                                      </div><!-- end grid row --> 
  	                  
  	                                      <label for="00Nj000000BKJk6">
  	                                          <h5>Tell us about your situation: <span class="optional">- Optional</span></h5>
  	                                      </label>
  	                                      <textarea placeholder="Tell us a little bit about what is going on..." cols="30" rows="6" id="00Nj000000BKJk6" name="00Nj000000BKJk6" type="text"></textarea>
  	                                   </div><!-- end #additional fields -->
  	                              </div><!-- end speed up feilds -->
  	                          </div><!-- end inner alt -->
  	                        
  	                          <div id="hidden-info">
  	                              <input id="00N0a00000CRYdN" maxlength="150" name="00N0a00000CRYdN" size="20" type="text" value="PPC LP (Skywood)" class="hide">
  	  
  	                              <select id="lead_source" name="lead_source" class="hide">
  	                                  <option value="Online" selected>Online</option>
  	                              </select>
  	                  
  	                              <textarea id="00Nj000000BKJjr" name="00Nj000000BKJjr" rows="1" type="text" value="http://talbottcampus.com/lp/" class="hide">http://talbottcampus.com/lp/</textarea>
  	                  
  	                              <select  id="00Nj000000BKJjt" name="00Nj000000BKJjt" title="Origin" class="hide">
  	                                  <option value="Web Form" selected>Web Form</option>
  	                              </select>
  	                  
  	                              <select  id="country_code" name="country_code" class="hide"><option value="US">US</option></select>
  	                          </div><!-- end hidden info -->
  	                        
  	                          
  	                          <div class="inner">
  	                              <div class="g-recaptcha" data-sitekey="6LehmHoUAAAAAAxLWRnOW1P3c2EftdBERtQ2XBEM" data-callback="recaptcha_callback"></div>
  	                              <div class="grid-x grid-margin-x">
  	                                  <div class="large-6 medium-6 cell">
  	                                      <input type="submit" class="button button--secure" value="Submit Form" disabled="true" data-ga-event data-ga-category="Online Form" data-ga-action="Form Submission"data-ga-label="Homepage VOB Submission">   
  	                                  </div><!-- end cell with button -->
  	                                  <div class="large-6 medium-6 cell">
  	                                      <div class="brand_block right"></div><!-- end logo for that site -->
  	                                  </div><!-- end cell with branded block -->
  	                              </div><!-- end row for button and logo -->
  	                          </div><!-- end inner -->
  	                      </form><!-- end the form --></div><!-- end #vob-holder -->	        <button class="close-button" data-ga-event data-ga-category="Video" data-ga-action="close modal" data-ga-label="Video" data-close aria-label="Close modal" type="button">
  	          <span aria-hidden>&times;</span>
  	        </button>
  	</div>
  </div>
  
  <div id="sticky-cta" class="sticky-cta show-for-medium">
  	<div class="inner">
  	<div class="grid-x">
  		<div class="cell">
  			<h2>Give us a call! We are here to help.</h2>
  		</div>
  		<div class="cell">
  			<div class="expanded button-group">
  		          <a href="tel:888-418-9657"
  		             class="InfinityNumber button button--primary"
  		             data-ict-discovery-number="888-418-9657"
  		             data-ict-silent-replacements="false"
  		             onClick="fbq('track', 'Lead', {
  		                        content_name: 'Talbott PPC', 
  		                        content_category: 'Landing Pages',
  		                        content_type: 'Phone Clicks'
  		                      });"
  		             data-ga-event
  		             data-ga-category="Phone Numbers"
  		             data-ga-action="Phone Clicks"
  		             data-ga-label="888-418-9657">
  		             888-418-9657
  		          </a>
  		        
  		        
  		    </div>
  		</div>
  	</div>
  	</div>
  </div>
    <script src="assets/js/app.js"></script>
</body>

</html>